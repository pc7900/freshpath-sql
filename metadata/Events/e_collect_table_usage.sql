DROP EVENT if exists metadata.e_collect_table_usage;
DELIMITER $$
CREATE DEFINER=`batchtrax`@`%` EVENT metadata.e_collect_table_usage
	ON SCHEDULE
		EVERY 1 DAY STARTS '2016-08-19 00:05:00'
	ON COMPLETION PRESERVE ENABLE
	COMMENT 'This event will collect table usage statistics every day.  Once a week it will be loaded to History table.'

-- Collect the metrics from the last 	
	DO begin
		call metadata.p_log_events(3, 0, 'Collect Table Usage', now());
		call metadata.p_acct_table_usage();
	end$$
		

