DROP EVENT if exists metadata.e_collect_table_usage_hist;
DELIMITER $$
CREATE DEFINER=`batchtrax`@`%` EVENT metadata.e_collect_table_usage_hist
	ON SCHEDULE
		EVERY 1 WEEK STARTS '2016-08-21 00:20:00'
	ON COMPLETION PRESERVE ENABLE
	COMMENT 'This event will collect table usage statistics History by account on a regular basis.'

-- Collect the metrics from the last 	
	DO begin
		call metadata.p_log_events(6, 0, 'Collect Table Usage History', now());
		call metadata.p_acct_table_usage_hist;
	end$$