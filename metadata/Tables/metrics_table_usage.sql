CREATE TABLE metadata.metrics_table_usage (
	db_name text NULL DEFAULT NULL,
	table_name text not null,
	action text not null, 
	acct_id INT(11) not null default 0,
	user_id INT(11) not null default 0,
	object_id int(11) not null default 0,
	created DATETIME NULL DEFAULT NULL
)
COMMENT='This Table contains a log of the actions of BatchMan users.  Not all actions will be logged.'
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;