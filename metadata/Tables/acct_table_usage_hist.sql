CREATE TABLE `acct_table_usage_hist` (
	`table_name` VARCHAR(50) NOT NULL,
	`acct_id` INT(11) NOT NULL,
	`run_date` DATE NOT NULL,
	`last_id` INT(11) NULL DEFAULT NULL,
	`row_cnt` INT(11) NOT NULL DEFAULT '0',
	`last_created` DATETIME NULL DEFAULT NULL,
	`last_created_by` INT(11) NULL DEFAULT NULL,
	UNIQUE INDEX `USI` (`acct_id`, `table_name`, `run_date`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
ROW_FORMAT=COMPACT
;
