CREATE TABLE metadata.configs_global (
	id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent Primary Key',
	column_name VARCHAR(40) NOT NULL DEFAULT '0' COMMENT 'The column name associated to the values',
	value VARCHAR(30) NOT NULL COMMENT 'valid values for the associated column',
	is_active TINYINT(4) NOT NULL DEFAULT '1' COMMENT 'Determines whether the type is active in current version',
	hover_text VARCHAR(250) NULL DEFAULT NULL COMMENT 'Description of value',
	created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'auto-populated upon Insert',
	modified TIMESTAMP NULL DEFAULT NULL COMMENT 'populate when modified',
	created_by INT(11) NOT NULL COMMENT 'FK to User Table',
	modified_by INT(11) NOT NULL COMMENT 'FK to User Table',
	PRIMARY KEY (id)
)
COMMENT='Global Configurations Table.  This table will contain configuration on a global level that populate dropdowns in the GUI.  This table will be maintained by BatchTrax Admins.\r\n'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=COMPACT
AUTO_INCREMENT=64
;
