CREATE TABLE `event_types` (
	`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent auto-increment ID',
	`event_type` VARCHAR(20) NULL DEFAULT NULL COMMENT 'Procedure, Function, Event, etc.',
	`schema_name` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Schema / database of the event',
	`event_name` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Name of the event Object',
	`created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date / Time the event type was created.',
	PRIMARY KEY (`id`)
)
COMMENT='This is a list of events types that can be tracked by the batchtrax logging system'
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=7
;
