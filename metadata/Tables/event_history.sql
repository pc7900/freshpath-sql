CREATE TABLE `event_history` (
	`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent auto-increment id.',
	`event_type_id` INT(11) NOT NULL COMMENT 'Name of Database',
	`event_code` INT(11) NOT NULL,
	`event_message` VARCHAR(255) NULL DEFAULT 'Successful' COMMENT 'Message associated to stamp.',
	`start_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'routine started',
	PRIMARY KEY (`id`)
)
COMMENT='This table will track auto or manually run routines that are part of the BatchTrax system... either operational, support or metadata processes.'
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=5
;
