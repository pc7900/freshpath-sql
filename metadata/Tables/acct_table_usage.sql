CREATE TABLE `acct_table_usage` (
	`table_name` VARCHAR(50) NULL DEFAULT NULL,
	`acct_id` INT(11) NULL DEFAULT NULL,
	`last_id` INT(11) NULL DEFAULT NULL,
	`row_cnt` INT(11) NULL DEFAULT NULL,
	`last_created` DATETIME NULL DEFAULT NULL,
	`last_created_by` INT(11) NULL DEFAULT NULL
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
