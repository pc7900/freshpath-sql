DROP PROCEDURE if exists metadata.p_acct_table_usage;
DELIMITER $$

CREATE DEFINER=`batchtrax`@`%` PROCEDURE metadata.p_acct_table_usage()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'This routine will capture usage of tables for all batchtrax accounts'

BEGIN
	DECLARE tbl_done, acct_done BOOLEAN DEFAULT FALSE; 
	declare v_tbl_name 			varchar(50);
	declare v_acct_id 			int;

	
	DECLARE tbl_csr CURSOR FOR SELECT 	TABLE_NAME 
									FROM 		information_schema.TABLES 
									WHERE 	table_schema	='batchtrax' 
									and 		TABLE_TYPE 		='BASE TABLE'
									and		TABLE_NAME not in ('accounts','users','ingredient_types','product_types')
									and 		TABLE_NAME not like 'global%';
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET tbl_done = true;
	
 	delete from metadata.acct_table_usage where 1 = 1;	-- delete all rows
-- set all the event variables.
	set @event_type = 2;
	set @event_code = 0;
	set @event_message = 'Success';
	set @event_start = now();

	open tbl_csr;
	tbl_loop: LOOP
		fetch from tbl_csr into v_tbl_name;
		if tbl_done then
			close tbl_csr;
			leave tbl_loop;
		end if;
		set acct_done = false;
		acct_block: begin
			declare v_row_cnt 			int;
			declare acct_csr cursor for select id from batchtrax.accounts;
			declare CONTINUE HANDLER FOR NOT FOUND SET acct_done = true;
			open acct_csr;
			acct_loop: loop
				fetch from acct_csr into v_acct_id;
				if acct_done then
					close acct_csr;
					leave acct_loop;
				end if;
/*  Capture the row counts for each account */
				set @row_cnt = null;
				SET @s = CONCAT('SELECT count(*) into @row_cnt FROM batchtrax.', v_tbl_name,' where account_id = ', v_acct_id);
				PREPARE stmt FROM @s;
				EXECUTE stmt;
				DEALLOCATE PREPARE stmt;
/*  Capture the last created date for each account */
				set @last_created = null;
				SET @s = CONCAT('SELECT id, created, created_by into @last_id, @last_created, @last_created_by ', 
									 ' FROM batchtrax.', v_tbl_name, 
									 ' where id in (Select max(id) from batchtrax.', v_tbl_name,  
	 								               ' where account_id =', v_acct_id, ')');
				PREPARE stmt FROM @s;
				EXECUTE stmt;
				DEALLOCATE PREPARE stmt;
/*  Insert into table   */
				insert into metadata.acct_table_usage (table_name, acct_id, last_id, row_cnt, last_created,last_created_by,run_date) 
				values (v_tbl_name, v_acct_id, @last_id, @row_cnt, @last_created, @last_created_by,date(curdate()));
			end loop acct_loop;
		end acct_block;
	end loop tbl_loop;
	call metadata.p_log_events(@event_type, @event_code, @event_message, @event_start);	
END
$$

			