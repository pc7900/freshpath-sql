create or replace view batch_man.v_prd_btch_dtl
as
select acct.name						acct_nm
,concat(prd.name,' ',pt.name)  	prd_type_nm
,prd.name								prd_nm
,prd.code								prd_cd
,pt.name									prd_type
-- ,pb.rcpe_mult_factor
,pb.location
,pb.batch_quantity
,mu.value								batch_units
,pb.batch_start
,pb.comments
,pb.batch_number
,pb.best_by_date
,prd.id									prd_id
,pb.id									prd_btch_id
,prd.account_id
,concat(acct.company_code,' ',prd.upc_item_reference,' ',prd.upc_check_digit) upc_cd
from batch_man.accounts acct
join batch_man.products prd 
on acct.id = prd.account_id
join	batch_man.product_types	pt
on		prd.product_type_id = pt.id
join batch_man.product_batches pb 
on prd.id = pb.product_id
left join batch_man.global_measure_units mu
on		pb.global_measure_unit_id = mu.id
