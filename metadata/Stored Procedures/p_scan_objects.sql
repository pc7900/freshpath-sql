DROP PROCEDURE if exists metadata.p_scan_objects;
DELIMITER $$

CREATE DEFINER=`batchtrax`@`%` PROCEDURE metadata.p_scan_objects()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'This routine will capture the Objects within enumerated databases'

BEGIN
	DECLARE obj_done BOOLEAN DEFAULT FALSE; 
	declare o_server	 			varchar(50);
	declare o_dbase				varchar(50);
	declare o_obj_type			varchar(50);
	declare o_obj_nm				varchar(50);
	declare o_create_dt			datetime;
	declare o_change_dt			datetime;

	
	DECLARE obj_csr CURSOR FOR SELECT Server, DBase, obj_type, obj_nm, ifnull(create_dt,now()) create_dt, change_dt 
									FROM 		metadata.v_tables
									where    dbase in ('BatchMan','metadata','batchtrax');
									
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET obj_done = true;
	
 	update metadata.objects 
 	 set detected = 0
	 where server = 'MySQL';	 -- update all MySQL rows.
-- set all the event variables.

	open obj_csr;
	obj_loop: LOOP
		fetch from obj_csr into o_server, o_dbase, o_obj_type, o_obj_nm, o_create_dt, o_change_dt;
		if obj_done then
			close obj_csr;
			leave obj_loop;
		end if;
/*  Insert into table   */
				insert into metadata.objects (Server, DBase, type, name, created, modified, detected, last_detected) 
				values (o_server, o_dbase,o_obj_type, o_obj_nm, o_create_dt, o_change_dt,1,now())
				ON DUPLICATE KEY 
				update detected = 1, last_detected = now();
				
	end loop obj_loop;
	
	INSERT INTO metadata.metrics_table_usage
    SET action = 'SCAN',
     db_name = 'metadata',
     table_name = 'p_scan_objects',
     acct_id = 3,
     user_id = 6,
     object_id = 551,
     comments = 'Scan of MySQL Objects',
     create_by = 6,
     action_dt = NOW() - INTERVAL 1 HOUR;
END
$$
			