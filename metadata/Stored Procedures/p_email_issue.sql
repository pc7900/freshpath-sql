DROP PROCEDURE if exists metadata.p_email_issue;
DELIMITER $$

CREATE DEFINER=`batchtrax`@`%` PROCEDURE metadata.p_email_issue(IN i_id int)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'This routine will create an email for a given issue and store it in the email_sends table.'

BEGIN
	DECLARE issue_done BOOLEAN DEFAULT FALSE; 
	declare o_id	 				int;
	declare o_comp_id				int;
	declare o_name					varchar(255);
	declare o_description		varchar(2000);
	declare o_link					varchar(255);
	declare o_create_dt			datetime;
	declare o_create_by			int;
	
	declare t_title				varchar(255);
	declare t_body					varchar(4000);
	declare t_link					varchar(255);
	declare t_bcc					varchar(255);
	declare t_class_id			int;
--	declare t_from					varchar(50);
	
	declare u_name					varchar(50);
	declare u_email				varchar(100);

	
 /* Get the email Template for an Issue */	
 	select 	email_title, email_body, email_link, email_class_id into t_title, t_body, t_link, t_class_id
 	from		metadata.email_templates
 	where		id = 2;
 	
 /*  Get the Issue info */ 	
 	SELECT   id, comp_id, name, description, link, create_dt, create_by 
 	into 		o_id, o_comp_id, o_name, o_description, o_link, o_create_dt, o_create_by
	FROM 		metadata.issue
	where    id = i_id;
   	
/*  Get the User Info from user entering the issue */
	select user_first_nm, user_email		into u_name, u_email
	from   BatchMan.BMan_User
	where  user_id = o_create_by;
	
/* Papulate the variable in the email Title */				
	set t_title = replace(t_title, '#user_name#', ifnull(u_name,''));
	set t_bcc = u_email;
	
/* Papulate the variables in the email body */				
	set t_body = replace(t_body, '#user_name#', ifnull(u_name,''));
	set t_body = replace(t_body, '#issue_id#', ifnull(o_id,'0'));
	set t_body = replace(t_body, '#issue_name#', ifnull(o_name,'Nothing'));
	set t_body = replace(t_body, '#description#', ifnull(o_description,'Nothing'));
		--		set t_body_new = concat(t_body_new, o_description);

/*  Insert into email_sends table   */
	insert into metadata.email_sends (email_id, email_class_id, email_to, email_cc, email_bcc, email_title, email_body, email_link, create_dt, create_by) 
	values (o_id, t_class_id, '', '', t_bcc, t_title, t_body, t_link, now(), o_create_by);

	
END
$$

			