DROP PROCEDURE if exists metadata.p_acct_table_usage_hist;
DELIMITER $$

CREATE DEFINER=`batchtrax`@`%` PROCEDURE metadata.p_acct_table_usage_hist()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'This routine will capture usage of tables for all batchtrax accounts into history table'

BEGIN
	
-- set all the event variables.
	set @event_type = 6;
	set @event_code = 0;
	set @event_message = 'Success';
	set @event_start = now();

	insert into metadata.acct_table_usage_hist 
				(table_name, acct_id, last_id, row_cnt, last_created, last_created_by, run_date) 
	select 	 table_name, acct_id, last_id, row_cnt, last_created, last_created_by, run_date
	from  metadata.acct_table_usage a;
-- Update all the row_nums for everything in the table.  This process can be improved down the line.			
	update	acct_table_usage_hist c
	join (
			select	run_date, rownum()	run_num
			from
			(
				select 	a.run_date
				from 		acct_table_usage_hist a
				group by run_date 
			) b
			order by run_date 
			) d
			on	c.run_date = d.run_date
			set c.run_num = d.run_num;
	-- log successful completion
	call metadata.p_log_events(@event_type, @event_code, @event_message, @event_start);	
END
$$