DROP procedure IF EXISTS metadata.p_maintenance;
DELIMITER $$
CREATE procedure metadata.p_maintenance
(
 		i_days 	INT
)
	COMMENT 'IN: days range from today '
BEGIN
-- Delete rows from metrics table usage over 	
	delete from metadata.metrics_table_usage 
		where action_dt > CURDATE() - interval 100 day;
	
END;
$$