DROP PROCEDURE if exists metadata.p_all_conversions;
DELIMITER $$

CREATE DEFINER=`batchtrax`@`%` PROCEDURE metadata.p_all_conversions()
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN


	drop table if exists batchtrax.product_batch_ingredients;
	drop table if exists batchtrax.product_batch_tasks;
	drop table if exists batchtrax.product_recipes;
	drop table if exists batchtrax.product_batches;
	drop table if exists batchtrax.workers;
	drop table if exists batchtrax.tasks;
	drop table if exists batchtrax.products;
	drop table if exists batchtrax.product_types;
	drop table if exists batchtrax.sales;
	drop table if exists batchtrax.ingredient_batches;
	drop table if exists batchtrax.vendors;
	drop table if exists batchtrax.brands;
	drop table if exists batchtrax.ingredients;
	drop table if exists batchtrax.ingredient_types;
-- Rename pair
	drop table if exists batchtrax.accounts_users;
	
	drop table if exists batchtrax.users;
	drop table if exists batchtrax.accounts;

	select 'All tables dropped.' as Message;
	/***********************
	  create accounts table
	************************/
	CREATE Table batchtrax.accounts
	select 	a.comp_id			id
	,			a.comp_nm			name
	,			1						global_account_type_id
	,			a.comp_desc			description
	,			''	 					street_address
	,			a.comp_city			city
	,			a.comp_state		state_code
	,			a.comp_zip			zip_code
	,			a.upc_comp_cd		company_code
	,			cast(a.create_dt as DATETIME)		created
	,			cast(a.change_dt as DATETIME)		modified
	,			Conversions.get_user_id(a.create_by) created_by
	,			Conversions.get_user_id(a.change_by) modified_by		
	from    	BatchMan.Company  a
	where comp_id > 0;
	
	ALTER TABLE batchtrax.accounts
		ADD PRIMARY KEY (id),
		comment 'Table for batchtrax accounts.',
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.  This will be the value that controls access to the other tables.' FIRST,
		CHANGE COLUMN global_account_type_id global_account_type_id INT(11) not null default 1 comment 'Type of account.',
		CHANGE COLUMN name name VARCHAR(100) NOT NULL COMMENT 'Company Name of the Account',
		CHANGE COLUMN zip_code zip_code VARCHAR(10) not null comment 'Zip code for the company / account.',
		CHANGE COLUMN company_code company_code VARCHAR(15) null comment '(IMPORTANT! but not required.  Standard for company code to be determined',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		CHANGE COLUMN street_address street_address VARCHAR(50) NULL DEFAULT '' COMMENT '(Optional) Street Address of Account',
		ADD CONSTRAINT FK_acct_glbl_acct_types FOREIGN KEY (global_account_type_id) REFERENCES global_account_types (id);
	
	/***********************
	  create users table
	************************/
	CREATE Table batchtrax.users
	select 	
		a.user_id				id
,		a.user_email			email
,		a.pwd						passwrd
,		a.user_first_nm		first_name
,		a.user_last_nm			last_name
,		a.comp_id_default		default_account_id
,		a.last_login
,		1							active
,		cast(a.create_dt as DATETIME)		created
,		cast(a.change_dt as DATETIME)		modified
,		Conversions.get_user_id(a.create_by) created_by
,		Conversions.get_user_id(a.change_by) modified_by		
	from	BatchMan.BMan_User a
	where user_id > 0;
	
	ALTER TABLE batchtrax.users
		comment 'The list of BatchTrax users.',
		ADD PRIMARY KEY (id),
		add unique index USI_usrs (email),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN email email VARCHAR(255) NOT NULL COMMENT 'Email for the user',
		CHANGE COLUMN first_name first_name VARCHAR(25) NOT NULL COMMENT 'First name of the user',
		CHANGE COLUMN last_name last_name VARCHAR(25) NOT NULL COMMENT 'Last name of the user',
		CHANGE COLUMN `password` `password` VARCHAR(255) NOT NULL COMMENT 'Encrypted password for user',
		CHANGE COLUMN default_account_id default_account_id int(11) not null default 0 comment 'This is the account that the user will start in each time he logs into batchtrax.',
		CHANGE COLUMN active active tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Indicates whether the User is active in batchtrax',
		change column last_login last_login datetime not null comment 'Records the last time this user logged in.',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_usr_default_acct FOREIGN KEY (default_account_id) references accounts (id); 
		
	/***********************
	  create accounts_users table
	************************/
	CREATE Table batchtrax.accounts_users
	SELECT 	id
	,			a.comp_id		account_id
	,			a.user_id
	,			0  				is_owner
	,			case when a.user_type = 'Admin' then 22 else 23 end  global_user_type_id 
	,			cast(a.create_dt as DATETIME)		created
	,			cast(a.change_dt as DATETIME)		modified
	,			Conversions.get_user_id(a.create_by) created_by
	,			Conversions.get_user_id(a.change_by) modified_by		
	from		BatchMan.Company_User a
	;
	ALTER TABLE batchtrax.accounts_users
		comment 'The cross reference list of BatchTrax users to accounts.  This table will also indicate the type of user within the account.',
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN user_id user_id INT(11) NOT NULL COMMENT 'FK to users Table',
		CHANGE COLUMN account_id account_id INT(11) NOT NULL COMMENT 'FK to accounts Table',
		CHANGE COLUMN global_user_type_id global_user_type_id INT(11) NOT NULL COMMENT 'FK to global_user_types Table',
		CHANGE COLUMN is_owner is_owner tinyint(1) NOT NULL COMMENT 'Indicates whether this user is the owner of the Account',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_acct_usr_usrs FOREIGN KEY (user_id) REFERENCES users (id),
		ADD CONSTRAINT FK_acct_usr_accts FOREIGN KEY (account_id) REFERENCES accounts (id),
		ADD CONSTRAINT FK_acct_usr_glbl_usr_types FOREIGN KEY (global_user_type_id) REFERENCES global_user_types (id); 
	/***********************
	  create brands table
	************************/
	CREATE Table batchtrax.brands
	select 	a.brnd_id		id
	,			a.comp_id		account_id
	,			a.brnd_nm		name
	,			a.brnd_link		url
	,			a.comments	
	,			cast(a.create_dt as DATETIME)		created
	,			cast(a.change_dt as DATETIME)		modified
	,			Conversions.get_user_id(a.create_by) created_by
	,			Conversions.get_user_id(a.change_by) modified_by		
	from		BatchMan.Brand a
	where brnd_id > 0
	;
	ALTER TABLE batchtrax.brands
		comment 'The account-specific list of brands for ingredients.',
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN account_id account_id INT(11) NOT NULL COMMENT 'FK to accounts Table',
		CHANGE COLUMN name name VARCHAR(100) NOT NULL COMMENT 'Name of the Brand',
		CHANGE COLUMN url url VARCHAR(100) NULL COMMENT 'URL of the Brand, if applicable',
		CHANGE COLUMN comments comments text NULL COMMENT 'Comments may hold addresses and / or contact information.',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_brnd_accts FOREIGN KEY (account_id) REFERENCES accounts (id);
	/***********************
	  create vendors table
	************************/
	CREATE Table batchtrax.vendors
	select 	
			a.vndr_id			id
	,		a.comp_id			account_id
	,		a.vndr_nm			name
	,		a.vndr_contact		contact_name
	,		a.vndr_phone		contact_phone
	,		''						contact_email
	,		a.comments	
	,		cast(a.create_dt as DATETIME)		created
	,		cast(a.change_dt as DATETIME)		modified
	,		Conversions.get_user_id(a.create_by) created_by
	,		Conversions.get_user_id(a.change_by) modified_by			
	from 	BatchMan.Vendor a
	where a.vndr_id > 0
	;
	ALTER TABLE batchtrax.vendors
		comment 'The account-specific list of vendors the account deals with to obtain ingredients.',
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN account_id account_id INT(11) NOT NULL COMMENT 'FK to accounts Table',
		CHANGE COLUMN name name VARCHAR(100) NOT NULL COMMENT 'Name of the Vendor',
		CHANGE COLUMN contact_phone contact_phone VARCHAR(20) NULL DEFAULT NULL COMMENT 'Vendor Phone',
		CHANGE COLUMN contact_email contact_email VARCHAR(50) NULL,
		CHANGE COLUMN comments comments text NULL COMMENT 'Comments',
		add column active tinyint(1) not null default 1 comment 'Vendor is active for this account.' AFTER comments,
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_vndr_accts FOREIGN KEY (account_id) REFERENCES accounts (id);
	/***********************
	  create ingredient_types table
	************************/
	CREATE Table batchtrax.ingredient_types
	select	0					id
	,			a.comp_id		account_id
	,			a.ingr_ctgry	name
	,			now()				created
	,			6					created_by
	from		BatchMan.Ingredient a
	where		a.comp_id > 0
	group by 2,3
;
	
	ALTER TABLE batchtrax.ingredient_types
		comment 'The account-specific list of ingredient types.',
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN account_id account_id INT(11) NOT NULL COMMENT 'FK to accounts Table',
		CHANGE COLUMN name name VARCHAR(100) NOT NULL COMMENT 'Name of the Ingredient Type',
		add column description text NULL DEFAULT NULL COMMENT 'Description of Ingredient type.' after name,
		add column active tinyint(1) not null default 1 comment 'Ingredient type is active for this account.'  AFTER description,
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		ADD 	 COLUMN modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.' AFTER created, 
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		ADD	 COLUMN modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.' AFTER created_by,
		ADD CONSTRAINT FK_ingr_type_accts FOREIGN KEY (account_id) REFERENCES accounts (id);

	/***********************
	  create ingredients table
	************************/
	CREATE Table batchtrax.ingredients
		select 		
				a.ingr_id			id
		,		a.comp_id			account_id
		,		a.ingr_nm			name
		,		Conversions.get_ingr_type(a.ingr_ctgry,a.comp_id) ingredient_type_id
		,		a.ingr_desc			description
		,		a.ingr_cd			code
		,		a.ingr_to_grams	grams_per_ounce
		,		1						active
		,		cast(a.create_dt as DATETIME)		created
		,		cast(a.change_dt as DATETIME)		modified
		,		Conversions.get_user_id(a.create_by) created_by
		,		Conversions.get_user_id(a.change_by) modified_by	
		from			BatchMan.Ingredient a
		left join	batchtrax.ingredient_types b
		on				a.ingr_ctgry = b.name
		and			a.comp_id = b.account_id
		where a.ingr_id > 0
;
	ALTER TABLE batchtrax.ingredients
		comment 'The account-specific list of ingredients that go into making the products.',
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN account_id account_id INT(11) NOT NULL COMMENT 'FK to accounts Table',
		CHANGE COLUMN name name VARCHAR(100) NOT NULL COMMENT 'Name of the Ingredient',
		change column code code varchar(20) not null default '-' comment 'Code for the ingredient.',
		change column description description text NULL DEFAULT NULL COMMENT 'Description of Ingredient.', 
		CHANGE COLUMN ingredient_type_id ingredient_type_id INT(11) NOT NULL comment 'FK to ingredient_types table.',
		CHANGE COLUMN grams_per_ounce grams_per_ounce INT(11) NOT NULL DEFAULT 0 COMMENT 'Conversion for an ounce of the ingredient to grams',
		CHANGE COLUMN active active tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Indicates whether the Ingredient is actively used by the Account in Recipes',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_ingr_ingr_types FOREIGN KEY (ingredient_type_id) REFERENCES ingredient_types (id),
		ADD CONSTRAINT FK_ingr_accts FOREIGN KEY (account_id) REFERENCES accounts (id);
	/***********************
	  create ingredient_batches table
	************************/
	CREATE Table batchtrax.ingredient_batches
		select 
	 		a.ingr_btch_id  		id
	,		a.ingr_id 				ingredient_id
	,		a.vndr_id		 		vendor_id
	,		a.brnd_id 				brand_id
	,		ifnull(a.vndr_lot_nbr,'')			lot_number
	,		a.ingr_btch				batch_number
	,		case when a.purch_dt is null or a.purch_dt = '0000-00-00' then
					'2029-12-31'
			else
					a.purch_dt
			end						purchase_date
	,		a.purch_qty				purchase_quantity
	,		Conversions.get_measure(a.purch_measure)	global_measure_unit_id 
	,		a.unit_qty				unit_quantity
	,		a.unit_price			unit_price
	,		a.purch_total_amt		purchase_total_amount
	,		a.best_by_dt			best_by_date
	,		a.comments				comments
	,		cast(a.create_dt as DATETIME)		created
	,		cast(a.change_dt as DATETIME)		modified
	,		Conversions.get_user_id(a.create_by) created_by
	,		Conversions.get_user_id(a.change_by) modified_by		
	from 	BatchMan.Ingredient_Batch	a
	join	batchtrax.ingredients		b
	on		a.ingr_id = b.id
	where a.ingr_btch_id > 0
	and 	a.vndr_id > 0
	and 	b.account_id > 0;
	
	ALTER TABLE batchtrax.ingredient_batches
		comment 'The account-specific list of ingredient batches.  Each ingredient needs to be procured from a vendor (or grown) and each batch needs to be recorded for tracking purposes.',
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN ingredient_id ingredient_id INT(11) NOT NULL COMMENT 'FK to ingredients Table',
		CHANGE COLUMN brand_id brand_id INT(11) NOT NULL DEFAULT 0 COMMENT '(Not Required). If present, FK to brands table',
		CHANGE COLUMN global_measure_unit_id global_measure_unit_id INT(11) NOT NULL DEFAULT 51 COMMENT 'FK to global_measure_units table',
		CHANGE COLUMN vendor_id vendor_id INT(11) NOT NULL DEFAULT 0 COMMENT 'FK to vendors table',
		change column lot_number lot_number varchar(100) not null default '' comment 'IMPORTANT! but not required.  Vendor provided lot number.  Populate if at all possible.',
		change column purchase_date purchase_date date not null comment '(Required) Date that the ingredient was purchased.',
		CHANGE COLUMN purchase_quantity purchase_quantity INT(11) NOT NULL DEFAULT 0 COMMENT '(Required). Should have an amount',
		CHANGE COLUMN unit_quantity unit_quantity INT(11) NOT NULL DEFAULT 0 COMMENT '(Required). Ex. if a dozen eggs bought, unit quantity would be 12.',
		CHANGE COLUMN comments comments text NULL COMMENT 'Comments',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_ingr_btch_vndrs FOREIGN KEY (vendor_id) REFERENCES vendors (id),
		ADD CONSTRAINT FK_ingr_btch_glbl_meas_unit FOREIGN KEY (global_measure_unit_id) REFERENCES global_measure_units (id), 
		ADD CONSTRAINT FK_ingr_btch_ingr FOREIGN KEY (ingredient_id) REFERENCES ingredients (id);
	/***********************
	  create workers  table
	************************/
	CREATE Table batchtrax.workers
		select	a.wrkr_ID	id
		,			a.comp_id	account_id
		,			a.worker		name
		,			a.active		active
		,			cast(a.create_dt as DATETIME)	created
		,			cast(a.change_dt as DATETIME)	modified
		,			Conversions.get_user_id(a.create_by) 		created_by
		,			Conversions.get_user_id(a.change_by) 		modified_by	
		from   BatchMan.Worker a
		where wrkr_id > 0
	;
	ALTER TABLE batchtrax.workers
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN account_id account_id INT(11) NOT NULL COMMENT 'FK to accounts Table',
		CHANGE COLUMN name name VARCHAR(100) NOT NULL COMMENT 'Name of the Worker',
		CHANGE COLUMN active active tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Indicates whether the Ingredient is actively used by the Account in Recipes',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_wrkr_accts FOREIGN KEY (account_id) REFERENCES accounts (id);
	/***********************
	  create product_types table
	************************/
	CREATE Table batchtrax.product_types
		select	0					id
		,			a.comp_id		account_id
		,			a.prd_type		name
		,			now()				created
		,			6					created_by
		,			null				modified
		,			null				modified_by
		from		BatchMan.Product a
		where		a.comp_id > 0
		group by 2,3
;
	
	ALTER TABLE batchtrax.product_types
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN account_id account_id INT(11) NOT NULL COMMENT 'FK to accounts Table',
		CHANGE COLUMN name name VARCHAR(100) NOT NULL COMMENT 'Name of the Product Type',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.' after created,
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_prd_type_accts FOREIGN KEY (account_id) REFERENCES accounts (id);
/***********************
	  create tasks  table
	************************/
	CREATE Table batchtrax.tasks
			select	a.task_id		id
		,			a.comp_id		account_id
		,			a.task			name
		,			a.task_ordr		ordr
		,			Conversions.get_prd_type_id(a.task_class,a.comp_id) 	task_class_id
		,			a.task_desc		description
		,			1					active
		,			cast(a.create_dt as DATETIME)		created
		,			cast(a.change_dt as DATETIME)		modified
		,			Conversions.get_user_id(a.create_by) 			created_by
		,			Conversions.get_user_id(a.change_by) 			modified_by	
		from		BatchMan.Task a
			where task_id > 0
;
	ALTER TABLE batchtrax.tasks
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN account_id account_id INT(11) NOT NULL COMMENT 'FK to accounts Table',
		change column task_class_id task_class_id int(11) not null default 0 comment 'FK to product_types table?',
		CHANGE COLUMN name name VARCHAR(100) NOT NULL COMMENT 'Name of the Task',
		change column description description text NULL DEFAULT NULL COMMENT 'Short description of the task',
		CHANGE COLUMN active active tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Indicates whether the row is active in batchtrax',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_task_accts FOREIGN KEY (account_id) REFERENCES accounts (id);
	/***********************
	  create products  table
	************************/
	CREATE Table batchtrax.products
			select 		
				a.prd_id					id
		,		a.comp_id				account_id
		,		a.prd_nm					name
		,		a.prd_cd					code
		,		Conversions.get_prd_type_id(a.prd_type, a.comp_id)	 product_type_id
		,		a.prd_desc				description
		,		a.rcpe_qty				recipe_quantity
		,		Conversions.get_measure(a.rcpe_measure) global_measure_unit_id
		,		a.upc_item_ref			upc_item_reference
		,		a.`upc check digit`	upc_check_digit
		,		1							active
		,		cast(a.create_dt as DATETIME)		created
		,		cast(a.change_dt as DATETIME)		modified
		,		Conversions.get_user_id(a.create_by) created_by
		,		Conversions.get_user_id(a.change_by) modified_by	
		from 	BatchMan.Product a
		where prd_id > 0
;
	ALTER TABLE batchtrax.products
		comment 'The list of products produced by the account.',
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN account_id account_id INT(11) NOT NULL COMMENT 'FK to accounts Table',
		change column product_type_id product_type_id int(11) not null comment 'FK to product_types table.',
		CHANGE COLUMN name name VARCHAR(50) NOT NULL COMMENT 'Name of the Product',
		change column description description text NULL DEFAULT NULL COMMENT 'Short description of the task',
		CHANGE COLUMN recipe_quantity recipe_quantity INT(11) NOT NULL DEFAULT 0 COMMENT 'Predicted volume/weight quantity of product to be produced based on the recipe',
		CHANGE COLUMN global_measure_unit_id global_measure_unit_id INT(11) NOT NULL DEFAULT 51 COMMENT 'FK to global_measure_units table.',
		CHANGE COLUMN active active tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Indicates whether the product is actively used by the Account in Recipes',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_prd_accts FOREIGN KEY (account_id) REFERENCES accounts (id),
		ADD CONSTRAINT FK_prd_glbl_meas_unit FOREIGN KEY (global_measure_unit_id) REFERENCES global_measure_units (id);
	/***********************
	  create product_recipes  table
	************************/
	CREATE Table batchtrax.product_recipes
		select	a.prd_ingr_id		id
		,			a.prd_id				product_id
		,			a.ingr_id			ingredient_id
		,			a.ingr_ordr			ingredient_order
		,			a.qty					quantity
		,			Conversions.get_measure(a.measure)	global_measure_unit_id 
		,			a.comments
		,			a.active				active
		,			cast(a.create_dt as DATETIME)		created
		,			cast(a.change_dt as DATETIME)		modified
		,			Conversions.get_user_id(a.create_by) created_by
		,			Conversions.get_user_id(a.change_by) modified_by				
		from 		BatchMan.Product_Recipe a
		join		BatchMan.Product b
		on			a.prd_id = b.prd_id
		where 	a.prd_ingr_id > 0
;
	ALTER TABLE batchtrax.product_recipes
	   comment 'The list of Ingredients that make up a product.',
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN product_id product_id INT(11) NOT NULL COMMENT 'FK to products Table',
		CHANGE COLUMN ingredient_id ingredient_id INT(11) NOT NULL COMMENT 'FK to ingredients Table',
		CHANGE COLUMN global_measure_unit_id global_measure_unit_id INT(11) NOT NULL DEFAULT 51 COMMENT 'FK to global_measure_units table. (Not used in initial release of BatchTrax)' after ingredient_id,
		CHANGE COLUMN ingredient_order ingredient_order INT(11) NOT NULL default 0 COMMENT 'Order that ingredient gets added to the recipe',
		CHANGE COLUMN comments comments text NULL COMMENT 'Comments',
		CHANGE COLUMN active active tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Indicates whether the Ingredient is actively used by the Account in Recipes.  For potential tracking purposes, if a product batch has ever been created with this ingredient you will want to set this ingredient to inactive instead of deleting the ingredient.',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_prd_rcpe_prds FOREIGN KEY (product_id) REFERENCES products (id),
		ADD CONSTRAINT FK_prd_rcpe_glbl_meas_unit FOREIGN KEY (global_measure_unit_id) REFERENCES global_measure_units (id),
		ADD CONSTRAINT FK_prd_rcpe_ingrs FOREIGN KEY (ingredient_id) REFERENCES ingredients (id);
	/***********************
	  create product_batches  table
	************************/
	CREATE Table batchtrax.product_batches
		select	a.prd_btch_id						id
		,			a.prd_id								product_id
		,			timestamp(a.process_dt, ifnull(a.fill_start_time,'07:00 AM'))	batch_start
		,			a.prd_btch							batch_number
		,			a.location				
		,			a.qty									batch_quantity
		,			Conversions.get_measure(a.measure)	global_measure_unit_id 
		,			cast(a.best_by_dt	 as date)			best_by_date
		,			a.comments
		,			a.rcpe_mult_factor				recipe_multiply_factor
		,			cast(a.create_dt as DATETIME)	created
		,			cast(a.change_dt as DATETIME)	modified
		,			Conversions.get_user_id(a.create_by) 		created_by
		,			Conversions.get_user_id(a.change_by) 		modified_by	
		from 		BatchMan.Product_Batch a
		join		BatchMan.Product b
		on			a.prd_id = b.prd_id
		where 	a.prd_btch_id > 0
	;
	ALTER TABLE batchtrax.product_batches
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN product_id product_id INT(11) NOT NULL COMMENT 'FK to products Table',
		CHANGE COLUMN global_measure_unit_id global_measure_unit_id INT(11) NOT NULL DEFAULT 51 COMMENT 'FK to global_measure_units table',
		CHANGE COLUMN comments comments text NULL COMMENT 'Comments',
		change column batch_start batch_start timestamp null comment 'The date and time when the batch is started.',
		CHANGE COLUMN location location VARCHAR(255) NOT NULL COMMENT 'The locaton where the Product Batch is created.',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_prd_btch_glbl_meas_unit FOREIGN KEY (global_measure_unit_id) REFERENCES global_measure_units (id),
		ADD CONSTRAINT FK_prd_btch_prds	FOREIGN KEY (product_id) REFERENCES products (id);
	/***********************
	  create product_batch_ingredients  table
	************************/
	CREATE Table batchtrax.product_batch_ingredients
		select	a.prd_btch_ingr_id					id
		,			a.prd_btch_id							product_batch_id
		,			a.prd_ingr_id							product_recipe_id
		,			a.ingr_btch_id							ingredient_batch_id
		,			''											comments
		,			ifnull(a.pbi_Qty,0)					ingredient_quantity
		,			Conversions.get_measure(a.pbi_Measure)			global_measure_unit_id 
		,			cast(a.create_dt as DATETIME)		created
		,			cast(a.change_dt as DATETIME)		modified
		,			Conversions.get_user_id(a.create_by) 			created_by
		,			Conversions.get_user_id(a.change_by) 			modified_by	
		from		BatchMan.Product_Batch_Ingredient a
		join		BatchMan.Product b
		on			a.prd_id = b.prd_id			
		where a.prd_btch_ingr_id > 0
;
	ALTER TABLE batchtrax.product_batch_ingredients
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN product_batch_id product_batch_id INT(11) NOT NULL COMMENT 'FK to product_batches table',
		CHANGE COLUMN product_recipe_id product_recipe_id INT(11) NOT NULL COMMENT 'FK to product_recipes table',
		CHANGE COLUMN ingredient_batch_id ingredient_batch_id INT(11) NOT NULL default 0 COMMENT '(Not required) Upon first creation will be zero.  Will then reference the ingredient_batches table.',
		CHANGE COLUMN comments comments text NULL COMMENT 'Comments',
		CHANGE COLUMN global_measure_unit_id global_measure_unit_id INT(11) NOT NULL DEFAULT 51 COMMENT 'FK to global_measure_units table',
		change column ingredient_quantity ingredient_quantity decimal(10,3) not null default 0 comment 'Amount of ingredient included in the product batch.',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_prd_btch_ingr_prd_btch FOREIGN KEY (product_batch_id) REFERENCES product_batches (id),
		ADD CONSTRAINT FK_prd_btch_ingr_glbl_meas_unit FOREIGN KEY (global_measure_unit_id) REFERENCES global_measure_units (id),
		ADD CONSTRAINT FK_prd_btch_ingr_prd_rcpe FOREIGN KEY (product_recipe_id) REFERENCES product_recipes (id);
	/***********************
	  create product_batch_tasks  table
	************************/
	CREATE Table batchtrax.product_batch_tasks
		select	a.btch_task_id		id
		,			a.prd_btch_id		product_batch_id
		,			a.task_id			task_id
		,			a.wrkr_id			worker_id
		,			a.comments
		,			a.value				measure_value
		,			cast(a.create_dt as DATETIME)	created
		,			cast(a.change_dt as DATETIME)	modified
		,			Conversions.get_user_id(a.create_by) 		created_by
		,			Conversions.get_user_id(a.change_by) 		modified_by	
		from 		BatchMan.Product_Batch_Task a
		left join BatchMan.Task	b
		on			a.task_id = b.task_id
		where a.btch_task_id > 0
;
	ALTER TABLE batchtrax.product_batch_tasks
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN product_batch_id product_batch_id INT(11) not null comment 'FK to product_batches table.',
		CHANGE COLUMN task_id task_id INT(11) not null comment 'FK to tasks table',
		CHANGE COLUMN worker_id worker_id INT(11) not null default 0 comment '(Not Required) Pseudo-FK to Worker table.',
		CHANGE COLUMN comments comments text NULL COMMENT 'Comments',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD CONSTRAINT FK_prd_btch_task_prd_btch FOREIGN KEY (product_batch_id) REFERENCES product_batches (id),
		ADD CONSTRAINT FK_prd_btch_task_tasks FOREIGN KEY (task_id) REFERENCES tasks (id);
	/***********************
	  create sales  table
	************************/
	CREATE Table batchtrax.sales
		select	a.sale_id								id
		,			a.comp_id								account_id
		,			a.sale_btch_id							batch_number
		,			a.cust_nm								customer_name
		,			a.sale_dt								sales_date
		,			a.sale_qty								quantity
		,			cast(a.sale_rate as dec(10,2))		rate
		,			cast(a.sale_amt as decimal(10,2))	total_amount
		,			a.trans_key								trans_key
		,			a.item_nbr								item_number
		,			cast(a.create_dt as DATETIME)		created
		,			cast(a.change_dt as DATETIME)		modified
		,			Conversions.get_user_id(a.create_by) 			created_by
		,			Conversions.get_user_id(a.change_by) 			modified_by	
		from		BatchMan.Sales a
		where a.sale_id > 0
;
	ALTER TABLE batchtrax.sales
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Non-Intelligent ID.' FIRST,
		CHANGE COLUMN account_id account_id INT(11) NOT NULL COMMENT 'FK to accounts Table',
		CHANGE COLUMN quantity quantity INT(11) NOT NULL DEFAULT 0 COMMENT 'Number of Units sold',
		change column rate rate decimal(10,2) not null comment 'Unit cost / rate for the transaction',
		CHANGE COLUMN created created datetime NOT NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN modified modified datetime NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN modified_by modified_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		add UNIQUE INDEX `USI_sales` (`batch_number`, `trans_key`, `rate`),
		ADD CONSTRAINT FK_sales_accts FOREIGN KEY (account_id) REFERENCES accounts (id);
	/***********************
	  rename tables as needed.
	************************/		
--		Rename Table batchtrax.account_users TO batchtrax.accounts_users;
	/***********************
	  reset Auto-Increment value for all Tables
	************************/
	call batchtrax.reset_autoincrement('batchtrax');
   /******************************************************
   */
	INSERT INTO metadata.metrics_table_usage
    SET action = 'Convert',
     db_name = 'Conversions',
     table_name = 'p_all_conversions',
     acct_id = 3,
     user_id = 6,
     object_id = 551,
     comments = 'Convert BatchMan to batchtrax tables',
     create_by = 6,
     action_dt = NOW() - INTERVAL 1 HOUR;
END