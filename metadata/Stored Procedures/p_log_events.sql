DROP procedure IF EXISTS metadata.p_log_events;
DELIMITER $$
CREATE procedure metadata.p_log_events
(
 		i_type_id 	INT
	,	i_code	 	int
	,	i_message	varchar(255)
	,	i_start		timestamp
)
	COMMENT 'IN: event parameters OUT: message failed / success'
BEGIN
	
	insert into metadata.event_history
						(event_type_id, event_code, event_message, start_time) 
				values (i_type_id, i_code, i_message, i_start);	
	
END;
$$