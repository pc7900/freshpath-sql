create or replace view metadata.v_batchman_tvfr
as
select a.id
,		 a.Server
,		 a.dbase
,		 case
			when a.alias = 'User' then
				'FrontEnd'
			else 
				'BackEnd' 
		 end  						which_end
,		 a.`type`  
,		 a.name
,		 a.class
,		 a.alias
,		 a.`status`
,		 a.re_link
,		 a.test_it
,		 a.description
,		 date_format(a.last_detected, '%m-%d-%y') last_detected
,		 case when a.detected = 0 then '' else 'Y' end detected
,		 date_format(a.created, '%m-%d-%y') created
,		 date_format(a.modified, '%m-%d-%y') modified
from   metadata.objects a
join	 metadata.objects_ui_usage b
on     a.id = b.object_id
where  b.ui_name = 'BatchMan.accdb'
union all
select c.id
,		 c.Server
,		 c.dbase
,		 'FrontEnd' 			which_end
,		 c.`type`  
,		 c.name
,		 c.class
,		 c.alias
,		 c.`status`
,		 c.re_link
,		 c.test_it
,		 c.description
,		 date_format(c.last_detected, '%m-%d-%y') last_detected
,		 case when c.detected = 0 then '' else 'Y' end detected
,		 date_format(c.created, '%m-%d-%y') created
,		 date_format(c.modified, '%m-%d-%y') modified
from   metadata.objects c
where  c.dbase = 'BatchMan.accdb'
order by 5, 2


