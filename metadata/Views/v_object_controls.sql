create or replace view metadata.v_object_controls
as
select 	a.Server
,			a.dbase
,			a.`type` as obj_type
,			a.name 	as obj_name
,			c.value	as obj_class
,			c.ordr
,			a.alias
,			case 
				when b.ctl_section = 1 then 'Header'
				when b.ctl_section = 0 then 'Detail'
				else 'Other'
			end																			 ctl_section
,			case when b.ctl_visible = 1 then 'Visible' else 'Hidden' end ctl_visible
,			b.ctl_tab_idx tab_idx
,			b.ctl_name
,			b.ctl_type
,			b.ctl_source
,			b.ctl_rowsource
,			a.source
,			a.description
,			b.comments
,			a.detected		obj_detect
,			b.detected		ctl_detect
,			b.obj_id
,			b.id
from   	metadata.objects a
join		metadata.object_controls b
on			a.id = b.obj_id
left join	metadata.vals_entity_class c
on			a.entity_class_id = c.id
where    b.ctl_type not in ('Box','Line','Empty Cell')
order by a.alias, a.name, b.ctl_visible desc, b.ctl_tab_idx, b.ctl_name