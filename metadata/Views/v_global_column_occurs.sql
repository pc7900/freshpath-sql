CREATE or REPLACE VIEW v_global_column_occurs
as
select	a.obj_type
,			a.obj_nm
,			a.col_nm
from		metadata.v_columns a
where exists
			(Select *
			 from 	metadata.configs_global
			 where 	column_name = a.col_nm)
and a.server = 'STAGE'
and a.dbase 	= 'batchtrax'