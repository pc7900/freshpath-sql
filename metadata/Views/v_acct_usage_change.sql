create or replace view metadata.v_acct_usage_change
as
select 	batchtrax.f_acct_name(a.acct_id)		acct_name
,			a.table_name
,			count(a.run_date)	weeks
,			max(a.row_cnt)	 - min(a.row_cnt)  additions
,			round((max(a.row_cnt) - min(a.row_cnt)) / count(a.run_date), 0) ave_per_wk
,			max(a.row_cnt) 	ending
from 		metadata.acct_table_usage_hist a
where 	acct_id <> 3
group by a.acct_id, a.table_name
having 	max(a.row_cnt)	 - min(a.row_cnt) > 0