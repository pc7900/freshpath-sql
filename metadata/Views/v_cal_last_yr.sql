create or replace view metadata.v_cal_last_yr
as
select *
from metadata.calendar_table a
where DATEDIFF(DATE(now()),a.dt) < 366