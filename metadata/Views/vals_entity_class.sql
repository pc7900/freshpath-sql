create or replace view metadata.vals_entity_class
as
Select 	a.id
,			a.value
,			a.ordr
,			a.description
,			a.column_name
from   	metadata.configs_global a
where  	a.column_name = 'entity_class_id'
