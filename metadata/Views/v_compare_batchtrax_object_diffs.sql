CREATE or REPLACE VIEW metadata.v_compare_batchtrax_object_diffs
as
select 
			if(sum(a.Stage) = 1,'Y','')	STAGE
,			if(sum(a.Test)  = 1,'Y','')	TEST	
,			a.obj_type
, 			a.obj_nm
,			a.dbase
from		metadata.v_compare_batchtrax_objects	a
group by a.dbase, a.obj_type, a.obj_nm
order by 3,4