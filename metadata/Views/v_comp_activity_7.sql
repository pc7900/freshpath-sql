create or replace view metadata.v_comp_activity_7
as
select   b.comp_Nm
,			a.class
,			a.alias
,			count(distinct user_id) users
,			sum(a.logins)		logins
,			sum(a.frm_open)	frm_open
,			sum(a.rpt_view) 	rpt_view
,			sum(a.rpt_prnt)	rpt_prnt
,			sum(a.inserts)		inserts
,			sum(a.updates)		updates
,			sum(a.deletes)		deletes
,			b.comp_id
from  	metadata.v_batchman_events_dim a
join		BatchMan.Company b
on			a.acct_id = b.comp_id
where 	a.acct_id <> 3
and		a.action_dt > Now() - interval 7 day
and		a.alias <> 'Navigator'
group by b.comp_nm
,			a.class
,			a.ordr
,			a.acct_id
,			a.alias
order by b.comp_abbrev
,			a.ordr
,			a.alias
,			a.acct_id