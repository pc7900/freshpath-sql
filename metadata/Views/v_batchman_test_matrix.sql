create or replace view metadata.v_batchman_test_matrix
as
Select		a.Server
,				a.class
,				a.dbase
,				a.`type`
,				a.name
,				a.alias
,				a.test_it
,				b.logins
,				b.fe_opens
,				b.fe_prints
,				b.be_inserts
,				b.be_updates
,				b.be_deletes
from   		metadata.v_batchman_tvfr a
left join 	metadata.v_admin_2_day_trigger b
on				a.alias = b.table_name
and			a.`type` = b.obj_type
where			a.dbase in ( 'BatchMan', 'metadata')
and			a.`type` = 'TABLE'
-- and			a.test_it <> 0
union all
Select 		c.Server
,				c.class
,				c.dbase
,				c.`type`
,				c.name
,				c.alias
,				c.test_it
,				d.logins
,				d.fe_opens
,				d.fe_prints
,				d.be_inserts
,				d.be_updates
,				d.be_deletes
from   		metadata.v_batchman_tvfr c
left join 	metadata.v_admin_2_day_trigger  d
on				c.name	 	= d.table_name
and			c.`type` 	= d.obj_type
where			c.dbase 		= 'BatchMan.accdb'
-- and			c.test_it <> 0
order by 	1, 2, 3, 5 desc, 4
