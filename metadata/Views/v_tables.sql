CREATE or REPLACE VIEW metadata.v_tables as
select 	'MySQL' 								Server
,			rout.ROUTINE_SCHEMA				DBase
,			cast(rout.ROUTINE_TYPE as char(20))	obj_type
,			rout.ROUTINE_NAME 				obj_nm
,			''										obj_engine
,			null									obj_rows
,			null									obj_incr
,			rout.ROUTINE_COMMENT 			obj_desc
,			rout.CREATED 						create_dt
,			rout.LAST_ALTERED				 	change_dt 
from 		information_schema.ROUTINES	rout
where 	rout.ROUTINE_SCHEMA IN ('batch_man')
union
select 	'MySQL' 								Server
,			trig.TRIGGER_SCHEMA				DBase
,			'TRIGGER'							obj_type
,			trig.TRIGGER_NAME 				obj_nm
,			''										obj_engine
,			null									obj_rows
,			null									obj_incr
,			concat('Capture ', trig.ACTION_TIMING, ' ', trig.EVENT_MANIPULATION, ' on ', trig.EVENT_OBJECT_TABLE) obj_desc
,			trig.CREATED 						create_dt
,			null								 	change_dt 
from 		information_schema.TRIGGERS	trig
where 	trig.TRIGGER_SCHEMA IN ('batch_man')
union
select 	'MySQL' 				as Server
,			tbl.TABLE_SCHEMA	as DBase
,			case when tbl.TABLE_TYPE = 'BASE TABLE' then 'TABLE' else 'VIEW' end AS obj_type
,			tbl.TABLE_NAME AS obj_nm
,			tbl.ENGINE AS obj_engine
,			tbl.TABLE_ROWS AS obj_rows
,			tbl.AUTO_INCREMENT AS obj_incr
,			tbl.TABLE_COMMENT AS obj_desc
,			tbl.CREATE_TIME AS create_dt
,			tbl.UPDATE_TIME AS change_dt 
from 		information_schema.TABLES tbl 
where 	tbl.TABLE_SCHEMA IN ('batch_man')
order by 1,2,3