CREATE or REPLACE VIEW global_ingredient_types
as
select   a.id
,			a.value
,			a.description
,			a.created
,			a.modified
from     configs_global a
where 	a.column_name = 'global_ingredient_type_id'
and   	a.is_active <> 0 
order by a.value 
;