create or replace view metadata.v_batchman_event_class_sum
as
select   a.class
,			a.alias
,			count(distinct acct_id) accts
,			count(distinct user_id) users
,			sum(a.logins)		logins
,			sum(a.frm_open)	frm_open
,			sum(a.rpt_view) 	rpt_view
,			sum(a.rpt_prnt)	rpt_prnt
,			sum(a.inserts)		inserts
,			sum(a.updates)		updates
,			sum(a.deletes)		deletes
from  	metadata.v_batchman_events_dim a
where 	a.acct_id <> 3
group by a.class
,			a.alias
order by a.class
,			a.alias