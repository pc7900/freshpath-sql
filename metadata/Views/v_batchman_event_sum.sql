create or replace view metadata.v_eventlog_base_summary
as
Select	c.comp_abbrev					abbrev
,			a.acct_id
,			a.db_name
,			date(a.action_dt)				action_dt						
,			case
				when a.table_name = 'User' then
					'FrontEnd'
				when a.db_name like '%accdb' then 
					'FrontEnd' 
				else 
					'BackEnd' 
			end  								which_end
,			a.table_name	
,			case
--				when a.table_name = 'User' then
--					'-LOGIN' 	
				when a.db_name = 'BatchMan' then 
					'TABLE' 
				when a.`action` in ('OpenReport','Print') then
					'REPORT'
				when a.`action` = 'OpenForm' then
					'FORM'
			end																						obj_type 
	
,			nullif(sum(case when a.action = 'Login'  then 1 else 0 end),0) 		logins
,			nullif(sum(case when a.`action` like 'Open%' then 1 else 0 end),0)	fe_opens
,			nullif(sum(case when a.`action` = 'Print' then 1 else 0 end),0)	 	fe_prints
,			nullif(sum(case when a.action = 'insert' then 1 else 0 end),0)			be_inserts
,			nullif(sum(case when a.action = 'update' then 1 else 0 end),0)			be_updates
,			nullif(sum(case when a.action = 'delete' then 1 else 0 end),0)			be_deletes
from   	metadata.metrics_table_usage a
join		BatchMan.Company c
on			a.acct_id = c.comp_id
where		acct_id > 0
and		a.action_dt > Now() - interval 180 day
and		a.db_name in ('BatchMan','BatchMan.accdb')
group by a.acct_id
,			a.db_name
,			a.table_name
,			date(a.action_dt)
order by 1,4 desc,6,5
