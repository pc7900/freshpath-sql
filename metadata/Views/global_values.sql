CREATE or REPLACE VIEW metadata.global_values
as
select   a.id
,			a.value
,			a.description
,			a.created
,			a.modified
,			a.column_name
from     configs_global a
where 	a.is_active <> 0 
order by a.value 
;