CREATE or REPLACE VIEW v_compare_batchtrax_diffs
as
select 
			if(sum(a.Stage) = 1,'Y','')	STAGE
,			if(sum(a.Test)  = 1,'Y','')	TEST	
,			a.dbase
,			a.obj_type
, 			a.obj_nm
,			a.col_nm
,			a.col_type
,			a.col_Key
,			a.Nulls
,			a.default_val
from		metadata.v_compare_batchtrax_detail	a
group by a.dbase, a.obj_type, a.obj_nm,  a.col_nm, a.col_type, a.col_Key, a.Nulls, a.default_val
having	sum(a.Stage) <> sum(a.Test)
order by 3,5,6