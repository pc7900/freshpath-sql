create or replace view metadata.v_cal_last_1000_days
as
select *
from metadata.calendar_table a
where DATEDIFF(DATE(now()),a.dt) < 1001