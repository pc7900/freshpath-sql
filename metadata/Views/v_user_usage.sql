create or replace view metadata.v_user_usage
as
select	concat(b.user_last_nm, ', ',b.user_first_nm) user_nm
,			b.comp_nm
,			b.user_email
,			count(a.id)			Logins
,			b.last_login
from		metadata.metrics_table_usage a
join		BatchMan.v_BMan_Comp_User	b
on			a.user_id = b.user_id
where		a.table_name = 'User'
and		a.`action` = 'Login'
and		b.active = 'Y'
and		a.user_id <> 2
group by 1, 2
order by 5 desc