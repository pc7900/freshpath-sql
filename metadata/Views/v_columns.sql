CREATE or replace view metadata.v_columns 
as
select	
	TABLE_SCHEMA db_name,
	TABLE_NAME ,
	COLUMN_NAME ,
	ORDINAL_POSITION col_ordr,
	COLUMN_DEFAULT 	col_dfault,
	IS_NULLABLE 		col_nulls,
	DATA_TYPE 			col_dtype,
	CHARACTER_MAXIMUM_LENGTH col_lngth,
	COLLATION_NAME ,
	COLUMN_TYPE 		col_type,
	case when COLUMN_KEY = 'PRI' then 'PK'
			when COLUMN_KEY = 'MUL' then 'FK'
			else null
	end						col_key,
	EXTRA 					col_extra,
	COLUMN_COMMENT			col_comment
from information_schema.`COLUMNS`
where TABLE_SCHEMA = 'batch_man'

