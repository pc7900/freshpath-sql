create or replace view v_event_history
as
select		a.start_time
,				b.event_type
,				b.event_name
,				b.schema_name	
from			metadata.event_history a
left join	metadata.event_types b
on				a.event_type_id = b.id
order by 	1 desc