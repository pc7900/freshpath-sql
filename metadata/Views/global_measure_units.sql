CREATE or REPLACE VIEW global_measure_units
as
select  	a.id
,			a.value
,			a.description	
,			a.created
,			a.modified
from    	configs_global a
where 	a.column_name = 'global_measure_unit_id'
and   	a.is_active <> 0 
order by a.value 
;