CREATE or REPLACE VIEW v_compare_batchtrax_objects
as
select  
		0  Stage
,    	1 	Test
,	 	dbase
,		obj_type
,   	obj_nm
from  metadata.v_tables
where	Server = 'TEST'
and	Dbase = 'batchtrax'
union
select  
		1 Stage
,    	0 Test
,	 	dbase
,		obj_type
,   	obj_nm
from  metadata.v_tables
where	Server = 'STAGE'
and	dbase = 'batchtrax'
order by 3,4,5