create or replace view metadata.v_batchman_company_matrix
as
Select   b.comp_id
,			b.comp_abbrev			abbrev
,			a.Server
,			a.dbase
,			a.which_end
,			a.`type`
,			a.name
,			a.alias
,			a.test_it
from		metadata.v_batchman_tvfr a
join		BatchMan.Company b
where		b.comp_id not in (0, 3)	
and		a.test_it = 1
order by 2,3,5,6