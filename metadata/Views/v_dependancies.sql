create or replace view metadata.v_dependancies
as
SELECT  tab.TABLE_SCHEMA 		db
,		  	tab.TABLE_NAME 		obj
,		  	views.TABLE_SCHEMA	dependant_db
,			views.TABLE_NAME	 	dependant_obj
FROM information_schema.`TABLES` AS tab 
INNER JOIN information_schema.VIEWS AS views 
ON instr(views.VIEW_DEFINITION , concat('`',tab.TABLE_NAME,'`')) > 0
where views.TABLE_SCHEMA not in ('Conversions')
-- where  tab.TABLE_SCHEMA not in ('Conversions')
union all 
SELECT  tab.TABLE_SCHEMA 		db
,		  	tab.TABLE_NAME 		obj
,		  	rtn.ROUTINE_SCHEMA	dependant_db
,			rtn.ROUTINE_NAME	 	dependant_obj
FROM information_schema.`TABLES` AS tab 
INNER JOIN information_schema.ROUTINES AS rtn
ON instr(rtn.ROUTINE_DEFINITION, concat(tab.TABLE_SCHEMA,'.',tab.TABLE_NAME)) > 0
-- where rtn.ROUTINE_SCHEMA in ('BatchMan','metadata')
where  tab.TABLE_SCHEMA not in ('Conversions')
ORDER BY 1,2,3,4