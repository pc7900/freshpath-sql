select	a.Server
,			a.dbase
,			a.name
,			case when b.ui_name is null then '-No Direct Connect' else b.ui_name end ui_name
,			a.`type`
from   metadata.objects a
left join metadata.objects_ui_usage b
on			a.id = b.object_id
where    a.`type` in ('TABLE', 'VIEW')
order by 2,3,4