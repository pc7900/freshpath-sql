create or replace view metadata.v_batchman_events_dim
as
Select	a.acct_id
,			a.user_id
,			c.ordr
,			c.value		class
,			b.alias
,			a.db_name
,			date(a.action_dt)				action_dt						
,			case
				when a.table_name = 'User' then
					'FrontEnd'
				when a.db_name like '%accdb' then 
					'FrontEnd' 
				else 
					'BackEnd' 
			end  								which_end
,			a.table_name																  			object_name
,			case
				when a.table_name = 'User' then
					'-LOGIN' 	
				when a.db_name = 'BatchMan' then 
					'TABLE' 
				when a.`action` in ('OpenReport','Print') then
					'REPORT'
				when a.`action` = 'OpenForm' then
					'FORM'
			end																						obj_type 
	
,			nullif(sum(case when a.action = 'Login'  then 1 else 0 end),0) 		logins
,			nullif(sum(case when a.`action` = 'OpenForm' then 1 else 0 end),0)	frm_open
,			nullif(sum(case when a.`action` = 'OpenReport' then 1 else 0 end),0)	rpt_view
,			nullif(sum(case when a.`action` = 'Print' then 1 else 0 end),0)	 	rpt_prnt
,			nullif(sum(case when a.action = 'insert' then 1 else 0 end),0)			inserts
,			nullif(sum(case when a.action = 'update' then 1 else 0 end),0)			updates
,			nullif(sum(case when a.action = 'delete' then 1 else 0 end),0)			deletes
from   	metadata.metrics_table_usage a
left join metadata.objects b
on			a.table_name = case when b.dbase in ('BatchMan','metadata') then b.alias else b.name end
and		a.db_name = b.dbase
and 		b.alias not in ('Login')
left join metadata.configs_global c
on			b.entity_class_id = c.id
where		acct_id > 0
and		a.table_name <> 'BMan_Navigator'
and		a.action_dt > Now() - interval 180 day
and		a.db_name in ('BatchMan','BatchMan.accdb')
group by a.acct_id
,			a.user_id
,			a.db_name
,			a.table_name
,			b.alias
,			c.ordr
,			c.value
,			date(a.action_dt)
