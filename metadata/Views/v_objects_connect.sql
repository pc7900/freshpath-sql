create or replace view metadata.v_objects_connect
as
select	ui.id
,			ui.ui_name
,			ui.attach_when
,			a.Server
,			a.dbase
,			a.type
,			a.name
,			a.status
,			a.re_link
,			a.description
,			ui.object_id
from metadata.objects a
join	metadata.objects_ui_usage ui
on 	a.id	= ui.object_id
order by 2,3 desc, a.name