create or replace view metadata.v_acct_usage_monthly
as
select   a.account
,			a.table_name
,			concat(DATE_FORMAT(a.week_of,'%Y-%m'),' (',date_format(a.week_of,'%b'),')') mnth
,			sum(a.new_entries)	occurs
from		v_acct_usage_weekly a
group by 1,3, 2
order by 1,2,3 desc
;