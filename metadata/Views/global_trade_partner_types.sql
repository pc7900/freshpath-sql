CREATE or REPLACE VIEW global_trade_partner_types
as
select   a.id
,			a.value
,			a.description
from     configs_global a
where 	a.column_name = 'global_trade_partner_type_id'
and   	a.is_active <> 0 
order by a.value 