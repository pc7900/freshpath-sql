create or replace view metadata.v_acct_usage_weekly
as
select 	batchtrax.f_acct_name(b.acct_id)  account
,			a.table_name
,			a.run_date			week_of
,			a.row_cnt - b.row_cnt	new_entries
from		metadata.acct_table_usage_hist a
join		metadata.acct_table_usage_hist b
on			a.acct_id 		= b.acct_id
and		a.table_name	= b.table_name
and		a.run_num - 1		= b.run_num 
where		a.row_cnt - b.row_cnt > 0
and 		a.table_name not in ('product_batch_ingredients', 'product_batch_tasks')
order by a.acct_id, a.table_name, a.run_date desc
