create or replace view metadata.v_batchtrax_stage
as
SELECT 
  b.server
, b.dbase
, b.obj_type
, b.obj_nm
, a.ordr
, a.col_nm
, a.col_Key
, a.Nulls
, a.col_type
, a.default_val
, case when a.col_desc = '' and a.obj_type = 'VIEW' then ' * derived' else a.col_desc end col_desc
, b.obj_rows
, b.obj_incr
, b.obj_desc
FROM metadata.v_tables  	b
left JOIN metadata.v_columns 	a
ON a.dbase = b.dbase 
AND a.server = b.server 
AND a.obj_nm = b.obj_nm
WHERE b.server="STAGE"
AND b.dbase in ('BatchMan','batchtrax','future')
ORDER BY b.DBase
,b.obj_type
, b.obj_nm
, a.ordr;