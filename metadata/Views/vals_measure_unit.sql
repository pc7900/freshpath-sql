create or replace view metadata.vals_measure_unit
as
Select 	a.id
,			a.value
,			a.ordr
,			a.description
,			a.column_name
from   	metadata.configs_global a
where  	a.column_name = 'measure_unit_id'