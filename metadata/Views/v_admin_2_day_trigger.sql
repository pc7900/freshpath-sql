create or replace view metadata.v_admin_2_day_trigger
as
Select 		b.db_name
,				b.table_name
,				case 	
					when b.db_name = 'BatchMan' then 
						'TABLE' 
					when b.`action` in ('OpenReport','Print') then
						'REPORT'
					when b.`action` = 'OpenForm' then
						'FORM'
				end																						obj_type 	
,				nullif(sum(case when b.action = 		'Login'  then 1 else 0 end),0) 	logins
,				nullif(sum(case when b.action like 	'Open%'  then 1 else 0 end),0)	fe_opens
,				nullif(sum(case when b.action = 		'Print'  then 1 else 0 end),0)	fe_prints
,				nullif(sum(case when b.action = 		'insert' then 1 else 0 end),0)	be_inserts
,				nullif(sum(case when b.action = 		'update' then 1 else 0 end),0)	be_updates
,				nullif(sum(case when b.action = 		'delete' then 1 else 0 end),0)	be_deletes	
from		 	metadata.metrics_table_usage b
where			b.acct_id = 3
and			b.db_name in ( 'BatchMan', 'metadata','BatchMan.accdb')
and			b.action_dt > Now() - interval 2 day
group by		1,2,3
