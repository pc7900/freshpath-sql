create or replace view metadata.v_batchman_obj_dtl
as
select	a.Server
,			a.dbase
,			a.type
,			a.name
,			a.status
,			b.ordr
,			b.col_nm
,			b.col_Key
,			b.default_val
,			b.nulls
,			b.col_type
,			a.description
,			b.col_desc
,			a.id		object_id
from metadata.objects a
join metadata.v_columns b
on   	a.dbase = b.dbase
and	a.name = b.obj_nm
join metadata.objects_ui_usage c
on		a.id	= c.object_id
where	c.ui_name = 'BatchMan.accdb'
order by a.dbase, a.type, a.name, b.ordr