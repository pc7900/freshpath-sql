create or replace view metadata.v_admin_trigger_track_mnthly
as
Select	concat(b.yr,'-',b.mnth) yr_mnth
,			c.comp_abbrev					abbrev
,			case
				when a.table_name = 'User' then
					'FrontEnd'
				when a.db_name like '%accdb' then 
					'FrontEnd' 
				else 
					'BackEnd' 
			end  								which_end
,			case 
				when a.table_name = 'User' then 
					'-Login'
				else 
					a.table_name	
			end acted_on
,			case 	
				when a.db_name = 'BatchMan' then 
					'TABLE' 
				when a.`action` in ('OpenReport','Print') then
					'REPORT'
				when a.`action` = 'OpenForm' then
					'FORM'
			end																						obj_type 	
,			nullif(sum(case when a.action = 'Login'  then 1 else 0 end),0) 		logins
,			nullif(sum(case when a.`action` like 'Open%' then 1 else 0 end),0)	fe_opens
,			nullif(sum(case when a.`action` = 'Print' then 1 else 0 end),0)	 	fe_prints
,			nullif(sum(case when a.action = 'insert' then 1 else 0 end),0)			be_inserts
,			nullif(sum(case when a.action = 'update' then 1 else 0 end),0)			be_updates
,			nullif(sum(case when a.action = 'delete' then 1 else 0 end),0)			be_deletes
from   	metadata.metrics_table_usage a
join			metadata.calendar_table b
on				date(a.action_dt) = b.dt
join		BatchMan.Company c
on			a.acct_id = c.comp_id
where		acct_id = 3
and		a.db_name in ('BatchMan','BatchMan.accdb')
group by b.yr
, 			b.mnth
,			a.acct_id
,			a.db_name
,			a.table_name
order by 1 desc, 3 desc,4