create or replace view BatchMan.v_navigate
as
select  	a.ordr				hi_ordr
,			a.object_alias		hi_name
,			a.object_desc		hi_desc
,			b.ordr				low_ordr
,			d.value				form_class
,			b.object_alias		low_name
,			b.object_desc
from		BatchMan.v_BMan_Obj_Parent a
join		BatchMan.v_BMan_Object 		b
on			a.nav_id = b.parent_id
join		metadata.objects				c
on			b.object_nm	= c.name
join		metadata.configs_global		d
on			c.entity_class_id	= d.id
where		a.area = 'User'
and		c.`type` = 'Form'
and		c.dbase = 'BatchMan.accdb'
order by 1,3