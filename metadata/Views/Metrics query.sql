Select	date(a.action_dt)  act_dt
,			time(a.action_dt)  act_tm
,			c.comp_abbrev			cmpny
,			concat(b.user_first_nm, ' ', substr(b.user_last_nm,1,1))	user_nm
,			a.table_name
,			d.class
,			a.`action`
,			a.comments
,			a.create_by
,			a.create_by
,			a.user_id		
from		metadata.metrics_table_usage a
join		metadata.objects d
on			a.table_name = d.name
and		a.db_name		= d.dbase
left join BatchMan.BMan_User b
on			a.user_id = b.user_id
left join BatchMan.Company c
on			a.acct_id = c.comp_id
where		action_dt > Now() - interval 72 hour
and not (a.table_name = 'aa_Login_Form' and a.`action` = 'Login')
and      d.test_it <> 0
order by a.action_dt desc