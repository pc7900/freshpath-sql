CREATE or REPLACE VIEW v_compare_batchtrax_detail
as
select  
		0  Stage
,    	1 	Test
,	 	dbase
,		obj_type
,   	obj_nm
,   	col_nm
,   	ordr
,   	col_type
,   	col_Key
,   	Nulls
,		default_val
from  v_columns
where	Server = 'TEST'
and	Dbase = 'batchtrax'
union
select  
		1 Stage
,    	0 Test
,	 	dbase
,		obj_type
,   	obj_nm
,   	col_nm
,   	ordr
,   	col_type
,   	col_Key
,   	Nulls
,		default_val
from  v_columns
where	Server = 'STAGE'
and	dbase = 'batchtrax'
order by 3,4,5