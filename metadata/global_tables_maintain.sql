/***********************
  create global_account_types table
************************/
drop table if exists batchtrax.global_account_types;

CREATE Table batchtrax.global_account_types
select * from metadata.global_account_types
where id > 0;

ALTER TABLE batchtrax.global_account_types
	ADD PRIMARY KEY (id);
	
/***********************
  create  global_container_types table
************************/
drop table if exists batchtrax.global_container_types;

CREATE Table batchtrax.global_container_types
select * from metadata.global_container_types
where id > 0;

ALTER TABLE batchtrax.global_container_types
	ADD PRIMARY KEY (id);
	
/***********************
  create  global_ingredient_types table
************************/
drop table if exists batchtrax.global_ingredient_types;

CREATE Table batchtrax.global_ingredient_types
select * from metadata.global_ingredient_types
where id > 0;

ALTER TABLE batchtrax.global_ingredient_types
	ADD PRIMARY KEY (id);
	
/***********************
  create  global_measure_units table
************************/
drop table if exists batchtrax.global_measure_units;

CREATE Table batchtrax.global_measure_units
select * from metadata.global_measure_units
where id > 0;

ALTER TABLE batchtrax.global_measure_units
	ADD PRIMARY KEY (id);
	
/***********************
  create  global_user_types table
************************/
drop table if exists batchtrax.global_user_types;

CREATE Table batchtrax.global_user_types
select * from metadata.global_user_types
where id > 0;

ALTER TABLE batchtrax.global_user_types
	ADD PRIMARY KEY (id);
	
