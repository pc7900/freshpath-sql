DROP PROCEDURE if exists mnts.p_create_id_uuid_xref;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE mnts.p_create_id_uuid_xref()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'This routine will capture the Auto Increment values that are used in foreign keys for conversion to uuids'

BLOCK1: BEGIN

	declare o_ent_nm				varchar(36);
	DECLARE obj_done BOOLEAN DEFAULT FALSE; 
	DECLARE obj_csr CURSOR FOR 
			SELECT 	a.ent_nm 
			FROM 		mnts.v_ents_dtl a
			where		a.ent_grp_nm = 'batch_man'
			and		a.ent_type = 'TABLE'
			and		a.ent_class in ('Configure','User','Global')
			;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET obj_done = true;
	
 	update metadata.id_to_uuid_xref
 	 set detected = 0;	-- update all MySQL rows.

	open obj_csr;
	obj_loop: LOOP
		fetch from obj_csr into o_ent_nm;
		if obj_done then
			close obj_csr;
			leave obj_loop;
		end if;
/*  Insert into table   */

		BLOCK2:BEGIN
		
			declare k_id			int;
			declare k_uuid			varchar(36);
			declare key_done boolean default false;
			DECLARE key_csr CURSOR FOR
				select  * from temp_tbl;
			DECLARE CONTINUE HANDLER FOR not found SET key_done = 1;	
			
			DROP TABLE IF EXISTS temp_tbl;
			SET @s = concat('CREATE TEMPORARY TABLE temp_tbl AS SELECT id, uuid() as uuid FROM batch_man.', o_ent_nm);

			PREPARE stmt FROM @s;
			EXECUTE stmt;
			DEALLOCATE PREPARE stmt;
			
		

			open key_csr;
			key_loop: LOOP
				fetch from key_csr into k_id, k_uuid;
				if key_done then
					close key_csr;
					leave key_loop;
				end if;
			
				insert into metadata.id_to_uuid_xref (id_val, id_name, uuid, detected, detected_at) 
				values (k_id, o_ent_nm, k_uuid, 1, now())
				ON DUPLICATE KEY 
				update 	detected = 1, 
							detected_at = now() - INTERVAL 1 HOUR;
			
			end loop key_loop;
		END BLOCK2;		
	end loop obj_loop;
end BLOCK1; 	

$$
			