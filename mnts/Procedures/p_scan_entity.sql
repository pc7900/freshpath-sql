DROP PROCEDURE if exists mnts.p_scan_entity;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE mnts.p_scan_entity()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'This routine will capture the Objects within enumerated databases'

BEGIN
	DECLARE obj_done BOOLEAN DEFAULT FALSE; 
	declare o_ent_grp_id 		int;
	declare o_dbase				varchar(50);
	declare o_obj_type			varchar(50);
	declare o_obj_nm				varchar(50);
	declare o_obj_desc			varchar(255);
	declare o_created_at			datetime;
	declare o_updated_at			datetime;

	
	DECLARE obj_csr CURSOR FOR 
			SELECT 	a.ent_grp_id, a.obj_type, a.obj_nm, a.obj_desc, a.created_at, a.updated_at 
			FROM 		mnts.v_ents a
			;
									
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET obj_done = true;
	
 	update mnts.ent
 	 set detected = 0;	-- update all MySQL rows.

	open obj_csr;
	obj_loop: LOOP
		fetch from obj_csr into o_ent_grp_id, o_obj_type, o_obj_nm, o_obj_desc, o_created_at, o_updated_at;
		if obj_done then
			close obj_csr;
			leave obj_loop;
		end if;
/*  Insert into table   */
				insert into mnts.ent (ent_grp_id, ent_type, ent_nm, ent_desc, created_at, updated_at, detected, last_detected) 
				values (o_ent_grp_id, o_obj_type, o_obj_nm, o_obj_desc, o_created_at, o_updated_at, 1, now())
				ON DUPLICATE KEY 
				update 	detected = 1, 
							last_detected = now() - INTERVAL 1 HOUR;

				
	end loop obj_loop;
	
	INSERT INTO mnts.mnts_log
    		SET action = 'Scan Ents',
    			ent_id = 814,
    			sprnt_id = 0,
    			action_id = 0,
    			comments = 'Scan Server Entities',
     			create_dt = NOW() - INTERVAL 1 HOUR,
     			create_by = 2
     		;
END
$$
			