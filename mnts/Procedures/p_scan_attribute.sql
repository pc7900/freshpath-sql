DROP PROCEDURE if exists mnts.p_scan_attribute;
DELIMITER $$

CREATE DEFINER=`batchtrax`@`%` PROCEDURE mnts.p_scan_attribute()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'This routine will capture the columns / attributes for pertinent MNTS schemas'

BEGIN
	DECLARE obj_done BOOLEAN DEFAULT FALSE; 
	declare o_ent_id 				int;
	declare o_ordr					int;
	declare o_nm					varchar(50);
	declare o_key					varchar(10);
	declare o_dflt					varchar(50);
	declare o_nullable			varchar(5);
	declare o_datatype			varchar(50);
	declare o_len					int;
	declare o_format				varchar(50);
	declare o_desc					varchar(1000);
	declare o_created_at			datetime;
	declare o_updated_at			datetime;

	
	DECLARE obj_csr CURSOR FOR 
			SELECT 	ent_id, ordr, col_nm, col_key, default_val, nulls, col_dtype, col_lngth,col_type,col_desc
			FROM 		mnts.v_col_scan;
									
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET obj_done = true;
	
 	update mnts.attr 
 	 set detected = 0
	  where ent_id in
	  		(SELECT 		b.ent_id
				FROM 		mnts.ent_grp a
				join		mnts.ent b
				on			a.ent_grp_id = b.ent_grp_id
				where    a.srvr_id = 1
			);   -- update all MySQL rows

-- set all the event variables.

	open obj_csr;
	obj_loop: LOOP
		fetch from obj_csr into o_ent_id, o_ordr, o_nm, o_key, o_dflt, o_nullable, o_datatype, o_len, o_format, o_desc;
		if obj_done then
			close obj_csr;
			leave obj_loop;
		end if;
/*  Insert into table   */
				insert into mnts.attr (ent_id, attr_ordr, attr_nm, attr_key, attr_datatype, attr_format, attr_len, attr_dflt, attr_nullable, attr_desc, detected, last_detected) 
				values (o_ent_id, o_ordr, o_nm, o_key, o_datatype, o_format, o_len, o_dflt, o_nullable, o_desc, 1, now())
				ON DUPLICATE KEY 
				update detected = 1
				, last_detected = now()  - INTERVAL 1 HOUR
				, attr_desc = o_desc;
				
	end loop obj_loop;
	
	INSERT INTO mnts.mnts_log
    		SET action = 'Scan Attrs',
    			ent_id = 807,
    			sprnt_id = 0,
    			action_id = 0,
    			comments = 'Scan Server Attributes',
     			create_dt = NOW() - INTERVAL 1 HOUR,
     			create_by = 2
     		;
END
$$
			
