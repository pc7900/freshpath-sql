DROP PROCEDURE if exists mnts.p_convert_packaging;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE mnts.p_convert_packaging()
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN

/*

 */
	INSERT INTO mnts.mnts_log
    		SET action = 'Convert',
    			ent_id = 0,
    			sprnt_id = 0,
    			action_id = 0,
    			comments = 'Sprint - Packaging',
     			create_dt = NOW() - INTERVAL 1 HOUR,
     			create_by = 2
     		;
   select 'All tables created.' as Message;
END