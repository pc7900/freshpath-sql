DROP PROCEDURE if exists batch_man.reset_autoincrement;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE batch_man.reset_autoincrement (IN `schemaName` varchar(255))
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT ''
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE o_name VARCHAR(255);
    DECLARE o_table VARCHAR(255);
    DECLARE cur1 CURSOR FOR SELECT COLUMN_NAME, TABLE_NAME FROM information_schema.`COLUMNS` WHERE extra LIKE '%auto_increment%' and table_schema=schemaName;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cur1;
    read_loop: LOOP
     FETCH cur1 INTO o_name, o_table;

   IF done THEN
       LEAVE read_loop;
   END IF;

  	set @qry1 = concat('SELECT MAX(`',o_name,'`) + 1 as autoincrement FROM `',o_table,'` INTO @ai'); 
  	PREPARE stmt1 FROM @qry1;
  	EXECUTE stmt1;

  	IF @ai IS NOT NULL THEN
    	set @qry2 = concat('ALTER TABLE `',o_table,'` AUTO_INCREMENT = ', @ai);
   	PREPARE stmt2 FROM @qry2;
   	EXECUTE stmt2;
  	END IF;

	END LOOP;
	select 'Auto-Increment Reset completed' as Message;
	CLOSE cur1;
 END
 $$