DROP PROCEDURE if exists mnts.p_convert_uuid;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE mnts.p_convert_uuid()
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN

	CALL setupProcLog();
	
	drop table if exists whatsfresh.product_batch_ingredients;
	drop table if exists whatsfresh.product_batch_tasks;
	drop table if exists whatsfresh.product_recipes;
	drop table if exists whatsfresh.product_batches;
	drop table if exists whatsfresh.ingredient_batches;
	
	drop table if exists future.product_batch_ingredients;
	drop table if exists future.product_batch_tasks;
	drop table if exists future.product_recipes;
	drop table if exists future.product_batches;
	drop table if exists future.ingredient_batches;

CALL procLog('All Tables Dropped');

	/***********************
	  create ingredient_batches table
	************************/
	CREATE Table whatsfresh.ingredient_batches
		select 
	 		a.ingr_btch_id  		id_old
	,		uuid_to_bin(uuid()) 	id
	,		a.ingr_id 				ingredient_id
	,		a.vndr_id		 		vendor_id
	,		a.brnd_id 				brand_id
	,		ifnull(a.vndr_lot_nbr,'')			lot_number
	,		a.ingr_btch				batch_number
	,		case when a.purch_dt is null or a.purch_dt = '0000-00-00' then
					'2029-12-31'
			else
					a.purch_dt
			end						purchase_date
	,		a.purch_qty				purchase_quantity
	,		BatchMan.get_measure(a.purch_measure)	global_measure_unit_id 
	,		a.unit_qty				unit_quantity
	,		a.unit_price			unit_price
	,		a.purch_total_amt		purchase_total_amount
	,		a.best_by_dt			best_by_date
	,		a.comments				comments
	,			a.create_dt			created_at
	,			a.change_dt 		updated_at
	,			BatchMan.get_user_id(a.create_by) created_by
	,			BatchMan.get_user_id(a.change_by) updated_by		
	from 	BatchMan.Ingredient_Batch	a
	join	whatsfresh.ingredients		b
	on		a.ingr_id = b.id
	where a.ingr_btch_id > 0
	and 	a.vndr_id > 0
	and 	b.account_id > 0
	order by a.purch_dt;
	
CALL procLog('ingredient_batches: Step 1 complete');
	
	ALTER TABLE whatsfresh.ingredient_batches
		comment 'The account-specific list of ingredient batches.  Each ingredient needs to be procured from a vendor (or grown) and each batch needs to be recorded for tracking purposes.',
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id_old id_old INT(11) NOT NULL COMMENT 'Non-Intelligent ID.' FIRST,
		change column id id binary(16)	not null comment 'Non-Intelligent binary UUID.',
		CHANGE COLUMN ingredient_id ingredient_id INT(11) NOT NULL COMMENT 'FK to ingredients Table',
		CHANGE COLUMN brand_id brand_id INT(11) NOT NULL DEFAULT 0 COMMENT '(Not Required). If present, FK to brands table',
		CHANGE COLUMN global_measure_unit_id global_measure_unit_id INT(11) NOT NULL DEFAULT 51 COMMENT 'FK to global_measure_units table',
		CHANGE COLUMN vendor_id vendor_id INT(11) NOT NULL DEFAULT 0 COMMENT 'FK to vendors table',
		change column lot_number lot_number varchar(100) not null default '' comment 'IMPORTANT! but not required.  Vendor provided lot number.  Populate if at all possible.',
		change column purchase_date purchase_date date not null comment '(Required) Date that the ingredient was purchased.',
		CHANGE COLUMN purchase_quantity purchase_quantity INT(11) NOT NULL DEFAULT 0 COMMENT '(Required). Should have an amount',
		CHANGE COLUMN unit_quantity unit_quantity INT(11) NOT NULL DEFAULT 0 COMMENT '(Required). Ex. if a dozen eggs bought, unit quantity would be 12.',
		CHANGE COLUMN comments comments text NULL COMMENT 'Comments',
		CHANGE COLUMN created_at created_at timestamp NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN updated_at updated_at timestamp NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN updated_by updated_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD 	 COLUMN deleted_at timestamp NULL COMMENT 'Populated with DATETIME upon soft delete of row.' AFTER updated_by,
		ADD 	 COLUMN deleted_by int(11) NULL COMMENT 'Populated with user_id upon soft delete of row.' AFTER deleted_at;
		
CALL procLog('ingredient_batches table created');

	/***********************
	  create product_recipes  table
	************************/
	CREATE Table whatsfresh.product_recipes
		select	a.prd_ingr_id		id_old
		,			uuid_to_bin(uuid()) 	id
		,			a.prd_id				product_id
		,			a.ingr_id			ingredient_id
		,			a.ingr_ordr			ingredient_order
		,			a.qty					quantity
		,			BatchMan.get_measure(a.measure)	global_measure_unit_id 
		,			a.comments
		,			case when active = 0 then now() else null end deleted_at
		,			a.create_dt			created_at
		,			a.change_dt 		updated_at
		,			BatchMan.get_user_id(a.create_by) created_by
		,			BatchMan.get_user_id(a.change_by) updated_by				
		from 		BatchMan.Product_Recipe a
		join		BatchMan.Product b
		on			a.prd_id = b.prd_id
		where 	a.prd_ingr_id > 0
;

CALL procLog('product_recipes: Step 1 complete');

	ALTER TABLE whatsfresh.product_recipes
	   comment 'The list of Ingredients that make up a product.',
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id_old id_old INT(11) NOT NULL COMMENT 'Non-Intelligent ID.' FIRST,
		change column id id binary(16)	not null comment 'Non-Intelligent binary UUID.',
		CHANGE COLUMN product_id product_id INT(11) NOT NULL COMMENT 'FK to products Table',
		CHANGE COLUMN ingredient_id ingredient_id INT(11) NOT NULL COMMENT 'FK to ingredients Table',
		CHANGE COLUMN global_measure_unit_id global_measure_unit_id INT(11) NOT NULL DEFAULT 51 COMMENT 'FK to global_measure_units table. (Not used in initial release of BatchTrax)' after ingredient_id,
		CHANGE COLUMN ingredient_order ingredient_order INT(11) NOT NULL default 0 COMMENT 'Order that ingredient gets added to the recipe',
		CHANGE COLUMN comments comments text NULL COMMENT 'Comments',
		CHANGE COLUMN created_at created_at timestamp NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN updated_at updated_at timestamp NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN updated_by updated_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		change COLUMN deleted_at deleted_at timestamp NULL COMMENT 'Populated with DATETIME upon soft delete of row.' AFTER updated_by,
		ADD 	 COLUMN deleted_by int(11) NULL COMMENT 'Populated with user_id upon soft delete of row.' AFTER deleted_at;
		
CALL procLog('product_recipes table created');

	/***********************
	  create product_batches  table
	************************/
	CREATE Table whatsfresh.product_batches
		select	a.prd_btch_id						id_old
		,			uuid_to_bin(uuid())				id
		,			a.prd_id								product_id
		,			timestamp(a.process_dt, ifnull(a.fill_start_time,'07:00 AM'))	batch_start
		,			a.prd_btch							batch_number
		,			a.location				
		,			a.qty									batch_quantity
		,			BatchMan.get_measure(a.measure)	global_measure_unit_id 
		,			cast(a.best_by_dt	 as date)			best_by_date
		,			a.comments
		,			a.rcpe_mult_factor				recipe_multiply_factor
	,			a.create_dt			created_at
	,			a.change_dt 		updated_at
	,			BatchMan.get_user_id(a.create_by) created_by
	,			BatchMan.get_user_id(a.change_by) updated_by	
		from 		BatchMan.Product_Batch a
		join		BatchMan.Product b
		on			a.prd_id = b.prd_id
		where 	a.prd_btch_id > 0
	;

CALL procLog('product_batches: Step 1 complete');

	ALTER TABLE whatsfresh.product_batches
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id_old id_old INT(11) NOT NULL COMMENT 'Non-Intelligent ID.' FIRST,
		change column id id binary(16)	not null comment 'Non-Intelligent binary UUID.',
		CHANGE COLUMN product_id product_id INT(11) NOT NULL COMMENT 'FK to products Table',
		CHANGE COLUMN global_measure_unit_id global_measure_unit_id INT(11) NOT NULL DEFAULT 51 COMMENT 'FK to global_measure_units table',
		CHANGE COLUMN comments comments text NULL COMMENT 'Comments',
		change column batch_start batch_start timestamp null comment 'The date and time when the batch is started.',
		CHANGE COLUMN location location VARCHAR(255) NOT NULL COMMENT 'The locaton where the Product Batch is created.',
		CHANGE COLUMN created_at created_at timestamp NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN updated_at updated_at timestamp NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN updated_by updated_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.',
		ADD 	 COLUMN deleted_at timestamp NULL COMMENT 'Populated with DATETIME upon soft delete of row.' AFTER updated_by,
		ADD 	 COLUMN deleted_by int(11) NULL COMMENT 'Populated with user_id upon soft delete of row.' AFTER deleted_at;
		
CALL procLog('product_batches table created');

	/***********************
	  create product_batch_ingredients  table
	************************/
	CREATE Table whatsfresh.product_batch_ingredients
		select	a.prd_btch_ingr_id					id_old
		,			uuid_to_bin(uuid())					id		
		,			a.prd_btch_id							prd_btch_old
		,			a.prd_ingr_id							prd_rcpe_old
		,			a.ingr_btch_id							ingr_btch_old
		,			''											comments
		,			ifnull(a.pbi_Qty,0)					ingredient_quantity
		,			BatchMan.get_measure(a.pbi_Measure)			global_measure_unit_id 
	,			a.create_dt			created_at
	,			a.change_dt 		updated_at
	,			BatchMan.get_user_id(a.create_by) created_by
	,			BatchMan.get_user_id(a.change_by) updated_by	
		from		BatchMan.Product_Batch_Ingredient a
		join		BatchMan.Product b
		on			a.prd_id = b.prd_id			
		where a.prd_btch_ingr_id > 0
;
	ALTER TABLE whatsfresh.product_batch_ingredients
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id_old id_old INT(11) NOT NULL COMMENT 'Non-Intelligent ID.' FIRST,
		change column id id binary(16)	not null comment 'Non-Intelligent binary UUID.',
		CHANGE COLUMN prd_btch_old prd_btch_old INT(11) NOT NULL COMMENT 'goner',		
		add COLUMN product_batch_id binary(16) NOT NULL COMMENT 'FK to product_batches table' after id,
		CHANGE COLUMN prd_rcpe_old prd_rcpe_old INT(11) NOT NULL COMMENT 'goner',		
		add COLUMN product_recipe_id binary(16) NOT NULL COMMENT 'FK to product_recipes table' after product_batch_id,
		CHANGE COLUMN ingr_btch_old ingr_btch_old INT(11) NOT NULL COMMENT 'goner',
		add COLUMN ingredient_batch_id binary(16) NOT NULL COMMENT '(Not required) Upon first creation will be zero.' after product_recipe_id,
		CHANGE COLUMN comments comments text NULL COMMENT 'Comments',
		CHANGE COLUMN global_measure_unit_id global_measure_unit_id INT(11) NOT NULL DEFAULT 51 COMMENT 'FK to global_measure_units table',
		change column ingredient_quantity ingredient_quantity decimal(10,3) not null default 0 comment 'Amount of ingredient included in the product batch.',
		CHANGE COLUMN created_at created_at timestamp NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN updated_at updated_at timestamp NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN updated_by updated_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.';


CALL procLog('product_batch_ingredients table created');

	/***********************
	  create product_batch_tasks  table
	************************/
	CREATE Table whatsfresh.product_batch_tasks
		select	a.btch_task_id			id_old
		,			uuid_to_bin(uuid())	id
		,			a.prd_btch_id			prd_btch_old	
		,			a.task_id				task_id
		,			a.wrkr_id				worker_id
		,			a.comments
		,			a.value					measure_value
	,			a.create_dt			created_at
	,			a.change_dt 		updated_at
	,			BatchMan.get_user_id(a.create_by) created_by
	,			BatchMan.get_user_id(a.change_by) updated_by	
		from 		BatchMan.Product_Batch_Task a
		left join BatchMan.Task	b
		on			a.task_id = b.task_id
		where a.btch_task_id > 0
;
	ALTER TABLE whatsfresh.product_batch_tasks
		ADD PRIMARY KEY (id),
		CHANGE COLUMN id_old id_old INT(11) NOT NULL COMMENT 'Non-Intelligent ID.' FIRST,
		change column id id binary(16)	not null comment 'Non-Intelligent binary UUID.',		
		CHANGE COLUMN prd_btch_old prd_btch_old INT(11) NOT NULL COMMENT 'goner',		
		add COLUMN product_batch_id binary(16) NOT NULL COMMENT 'FK to product_batches table' after id,
		CHANGE COLUMN task_id task_id INT(11) not null comment 'FK to tasks table',
		CHANGE COLUMN worker_id worker_id INT(11) not null default 0 comment '(Not Required) Pseudo-FK to Worker table.',
		CHANGE COLUMN comments comments text NULL COMMENT 'Comments',
		CHANGE COLUMN created_at created_at timestamp NULL COMMENT 'Auto-populated with DATETIME upon Insert',
		CHANGE COLUMN updated_at updated_at timestamp NULL COMMENT 'Populated with DATETIME upon modification of row.',
		CHANGE COLUMN created_by created_by INT(11) NOT NULL COMMENT 'auto-populated with user_id upon Insert',
		CHANGE COLUMN updated_by updated_by INT(11) NULL COMMENT 'Populated with user_id upon modification of row.';


CALL procLog('product_batch_tasks table created');


/* Update the Mapping Rows */

-- Product Batch id
update whatsfresh.product_batch_ingredients a
join		whatsfresh.product_batches b
on			a.prd_btch_old = b.id_old
	set a.product_batch_id = b.id	
where 1=1;

-- Product Recipe id
update whatsfresh.product_batch_ingredients a
join		whatsfresh.product_recipes b
on			a.prd_rcpe_old = b.id_old
	set a.product_recipe_id = b.id	
where 1=1;


--	Ingredient Batch id
update whatsfresh.product_batch_ingredients a
join		whatsfresh.ingredient_batches b
on			a.ingr_btch_old = b.id_old
	set a.ingredient_batch_id = b.id	
where 1=1;

update whatsfresh.product_batch_tasks a
join		whatsfresh.product_batches b
on			a.prd_btch_old = b.id_old
	set a.product_batch_id = b.id	
where 1=1;
		
CALL procLog('Start creating constraints');

ALTER TABLE whatsfresh.ingredient_batches	
	ADD CONSTRAINT FK_ingr_btch_vndrs FOREIGN KEY (vendor_id) REFERENCES vendors (id),
	ADD CONSTRAINT FK_ingr_btch_glbl_meas_unit FOREIGN KEY (global_measure_unit_id) REFERENCES global_measure_units (id), 
	ADD CONSTRAINT FK_ingr_btch_ingr FOREIGN KEY (ingredient_id) REFERENCES ingredients (id);

CALL procLog('ingredient batches constraints created');

ALTER TABLE whatsfresh.product_recipes	
	ADD CONSTRAINT FK_prd_rcpe_prds FOREIGN KEY (product_id) REFERENCES products (id),
	ADD CONSTRAINT FK_prd_rcpe_glbl_meas_unit FOREIGN KEY (global_measure_unit_id) REFERENCES global_measure_units (id),
	ADD CONSTRAINT FK_prd_rcpe_ingrs FOREIGN KEY (ingredient_id) REFERENCES ingredients (id);

CALL procLog('product recipes constraints created');
	
ALTER TABLE whatsfresh.product_batches
	ADD CONSTRAINT FK_prd_btch_glbl_meas_unit FOREIGN KEY (global_measure_unit_id) REFERENCES global_measure_units (id),
	ADD CONSTRAINT FK_prd_btch_prds	FOREIGN KEY (product_id) REFERENCES products (id);

CALL procLog('product batches constraints created');
	
ALTER TABLE whatsfresh.product_batch_ingredients
	ADD CONSTRAINT FK_prd_btch_ingr_glbl_meas_unit FOREIGN KEY (global_measure_unit_id) REFERENCES global_measure_units (id),
	ADD CONSTRAINT FK_prd_btch_ingr_prd_rcpe FOREIGN KEY (product_recipe_id) REFERENCES product_recipes (id),
	ADD CONSTRAINT FK_prd_btch_ingr_prd_btch FOREIGN KEY (product_batch_id) REFERENCES product_batches (id),
	ADD UNIQUE INDEX USI_prd_btch_ingr (product_batch_id, product_recipe_id, ingredient_batch_id);

CALL procLog('product batch tasks constraints created');
	
ALTER TABLE whatsfresh.product_batch_tasks
	ADD CONSTRAINT FK_prd_btch_task_prd_btch FOREIGN KEY (product_batch_id) REFERENCES product_batches (id),
	ADD CONSTRAINT FK_prd_btch_task_tasks FOREIGN KEY (task_id) REFERENCES tasks (id);

CALL procLog('All Done with Constraints');

ALTER TABLE whatsfresh.ingredient_batches
	drop column id_old;	
	
ALTER TABLE whatsfresh.product_recipes
	drop column id_old;
		
ALTER TABLE whatsfresh.product_batches
	drop column id_old;
		
ALTER TABLE whatsfresh.product_batch_ingredients
	drop column id_old,
	drop column prd_btch_old,
	drop column prd_rcpe_old,
	drop column ingr_btch_old;
	
ALTER TABLE whatsfresh.product_batch_tasks
	drop column id_old,
	drop column prd_btch_old;

commit;

CALL procLog('All Done dropping old IDs');	
				
call mnts.p_scan_entity;
call mnts.p_scan_attribute;			

CALL procLog('All Done with updating metadata');

   select 'All tables created.' as Message;
   call cleanup("end of proc");
END