DROP PROCEDURE if exists mnts.p_migrate_no_tmg;
DELIMITER $$

CREATE PROCEDURE mnts.p_migrate_no_tmg()
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Conversion script to capture data that will be migrated once TMG is converted.'
BEGIN
p_convert_uuid
	CALL setupProcLog();
	
	drop table if exists wf_migrate.product_batch_ingredients;
	drop table if exists wf_migrate.product_batch_tasks;
	drop table if exists wf_migrate.product_recipes;
	drop table if exists wf_migrate.product_batches;
	drop table if exists wf_migrate.workers;
	drop table if exists wf_migrate.tasks;
	drop table if exists wf_migrate.products;
	drop table if exists wf_migrate.product_types;
	drop table if exists wf_migrate.ingredient_batches;
	drop table if exists wf_migrate.vendors;
	drop table if exists wf_migrate.brands;
	drop table if exists wf_migrate.ingredients;
	drop table if exists wf_migrate.ingredient_types;

	drop table if exists wf_migrate.accounts_users;
	
	drop table if exists wf_migrate.users;
	drop table if exists wf_migrate.accounts;

CALL procLog('All Tables Dropped');
select 'All Tables Dropped' as Message;
	/***********************
	  create accounts table
	************************/
	CREATE Table wf_migrate.accounts
	select *
	from   whatsfresh.accounts
	where  id <> 1
;
	
CALL procLog('accounts table created');
	
	/***********************
	  create users table
	************************/
	CREATE Table wf_migrate.users
	select *
	from  whatsfresh.users		
	where  default_account_id <> 1
;
CALL procLog('users table created');	
	
	/***********************
	  create accounts_users table
	************************/
	CREATE Table wf_migrate.accounts_users
	select *
	from  whatsfresh.accounts_users
	where account_id <> 1
;
CALL procLog('accounts_users table created');

	/***********************
	  create brands table
	************************/
	CREATE Table wf_migrate.brands
	select 	*
	from		whatsfresh.brands
	where		account_id <> 1
;
CALL procLog('brands table created');

	/***********************
	  create vendors table
	************************/
	CREATE Table wf_migrate.vendors
	select * 	
	from 	whatsfresh.vendors 
	where account_id <> 1
;
CALL procLog('vendors table created');

	/***********************
	  create ingredient_types table
	************************/
	CREATE Table wf_migrate.ingredient_types
	select	*
	from	whatsfresh.ingredient_types
	where	account_id > 1
;
CALL procLog('ingredient_types table created');

	/***********************
	  create ingredients table
	************************/
	CREATE Table wf_migrate.ingredients
	select *	
	from	whatsfresh.ingredients a
	where a.ingredient_type_id in (select id from wf_migrate.ingredient_types)
;
CALL procLog('ingredients table created');

	/***********************
	  create ingredient_batches table
	************************/
	CREATE Table wf_migrate.ingredient_batches
	select *
	from 	whatsfresh.ingredient_batches	
	where ingredient_id in (select id from wf_migrate.ingredients)
;	
CALL procLog('ingredient_batches table created');

	/***********************
	  create workers  table
	************************/
	CREATE Table wf_migrate.workers
	select *	
	from   whatsfresh.workers 
	where account_id <> 1
;
CALL procLog('workers table created');

	/***********************
	  create product_types table
	************************/
	CREATE Table wf_migrate.product_types
	select *
	from whatsfresh.product_types
	where account_id <> 1
;
CALL procLog('product_types table created');

/***********************
	  create tasks  table
	************************/
	CREATE Table wf_migrate.tasks
	select	*
	from whatsfresh.tasks
	where product_type_id in (select id from wf_migrate.product_types)
;
CALL procLog('tasks table created');

	/***********************
	  create products  table
	************************/
	CREATE Table wf_migrate.products
	select *
	from whatsfresh.products
	where product_type_id in (select id from wf_migrate.product_types)		
;
CALL procLog('products table created');

	/***********************
	  create product_recipes  table
	************************/
	CREATE Table wf_migrate.product_recipes
	select * from whatsfresh.product_recipes
	where  product_id in (select id from wf_migrate.products)
;
CALL procLog('product_recipes table created');

	/***********************
	  create product_batches  table
	************************/
	CREATE Table wf_migrate.product_batches
	select * from whatsfresh.product_batches
	where  product_id in (select id from wf_migrate.products)
;
CALL procLog('product_batches table created');

	/***********************
	  create product_batch_ingredients  table
	************************/
	CREATE Table wf_migrate.product_batch_ingredients
	select * from whatsfresh.product_batch_ingredients
	where  product_batch_id in (select id from wf_migrate.product_batches)
;
CALL procLog('product_batch_ingredients table created');

	/***********************
	  create product_batch_tasks  table
	************************/
	CREATE Table wf_migrate.product_batch_tasks
	select * from whatsfresh.product_batch_tasks
	where  product_batch_id in (select id from wf_migrate.product_batches)
;
CALL procLog('product_batch_tasks table created');


	INSERT INTO mnts.mnts_log
    		SET action = 'Convert',
    			ent_id = 0,
    			sprnt_id = 0,
    			action_id = 0,
    			comments = 'Create wf_migrate.data',
     			create_dt = NOW() - INTERVAL 1 HOUR,
     			create_by = 2
     		;
   select 'All tables created.' as Message;
   call cleanup("end of proc");
END