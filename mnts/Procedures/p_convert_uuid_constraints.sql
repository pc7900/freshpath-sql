DROP PROCEDURE if exists mnts.p_convert_uuid_constraints;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE mnts.p_convert_uuid_constraints()
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN

CALL setupProcLog();

CALL procLog('Start creating constraints');

ALTER TABLE batch_man.ingredient_batches	
	ADD CONSTRAINT FK_ingr_btch_vndrs FOREIGN KEY (vendor_id) REFERENCES vendors (id),
	ADD CONSTRAINT FK_ingr_btch_glbl_meas_unit FOREIGN KEY (global_measure_unit_id) REFERENCES global_measure_units (id), 
	ADD CONSTRAINT FK_ingr_btch_ingr FOREIGN KEY (ingredient_id) REFERENCES ingredients (id);

CALL procLog('ingredient batches constraints created');

ALTER TABLE batch_man.product_recipes	
	ADD CONSTRAINT FK_prd_rcpe_prds FOREIGN KEY (product_id) REFERENCES products (id),
	ADD CONSTRAINT FK_prd_rcpe_glbl_meas_unit FOREIGN KEY (global_measure_unit_id) REFERENCES global_measure_units (id),
	ADD CONSTRAINT FK_prd_rcpe_ingrs FOREIGN KEY (ingredient_id) REFERENCES ingredients (id);

CALL procLog('product recipes constraints created');
	
ALTER TABLE batch_man.product_batches
	ADD CONSTRAINT FK_prd_btch_glbl_meas_unit FOREIGN KEY (global_measure_unit_id) REFERENCES global_measure_units (id),
	ADD CONSTRAINT FK_prd_btch_prds	FOREIGN KEY (product_id) REFERENCES products (id);

CALL procLog('product batches constraints created');
	
ALTER TABLE batch_man.product_batch_ingredients
	ADD CONSTRAINT FK_prd_btch_ingr_glbl_meas_unit FOREIGN KEY (global_measure_unit_id) REFERENCES global_measure_units (id),
	ADD CONSTRAINT FK_prd_btch_ingr_prd_rcpe FOREIGN KEY (product_recipe_id) REFERENCES product_recipes (id),
	ADD CONSTRAINT FK_prd_btch_ingr_prd_btch FOREIGN KEY (product_batch_id) REFERENCES product_batches (id),
	ADD UNIQUE INDEX USI_prd_btch_ingr (product_batch_id, product_recipe_id, ingredient_batch_id);

CALL procLog('product batch tasks constraints created');
	
ALTER TABLE batch_man.product_batch_tasks
	ADD CONSTRAINT FK_prd_btch_task_prd_btch FOREIGN KEY (product_batch_id) REFERENCES product_batches (id),
	ADD CONSTRAINT FK_prd_btch_task_tasks FOREIGN KEY (task_id) REFERENCES tasks (id);

CALL procLog('All Done with Constraints');

call cleanup("end of proc");
			
end