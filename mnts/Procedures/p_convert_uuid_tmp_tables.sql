DROP PROCEDURE if exists mnts.p_convert_uuid_tmp_tables;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE mnts.p_convert_uuid_tmp_tables()
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN

	CALL setupProcLog();
	
	drop table if exists batch_man.tmp_ingr_btch;
	drop table if exists batch_man.tmp_prd_btch_tasks;
	drop table if exists batch_man.tmp_prd_rcpe;
	drop table if exists batch_man.tmp_prd_btch;
	drop table if exists batch_man.tmp_ingr_btch;
	
	create temporary table batch_man.tmp_ingr_btch
	select 
	 		a.ingr_btch_id  		id_old
	,		uuid_to_bin(uuid()) 	id
	from 	BatchMan.Ingredient_Batch	a
	join	batch_man.ingredients		b
	on		a.ingr_id = b.id
	where a.ingr_btch_id > 0
	and 	a.vndr_id > 0
	and 	b.account_id > 0
	order by a.purch_dt;	
	
	CREATE temporary Table batch_man.tmp_prd_rcpe
		select	a.prd_ingr_id		id_old
		,			uuid_to_bin(uuid()) 	id
		from 		BatchMan.Product_Recipe a
		join		BatchMan.Product b
		on			a.prd_id = b.prd_id
		where 	a.prd_ingr_id > 0;
		
	CREATE temporary Table batch_man.tmp_prd_btch
		select	a.prd_btch_id						id_old
		,			uuid_to_bin(uuid())				id
		from 		BatchMan.Product_Batch a
		join		BatchMan.Product b
		on			a.prd_id = b.prd_id
		where 	a.prd_btch_id > 0;



end