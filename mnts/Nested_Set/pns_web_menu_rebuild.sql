DROP PROCEDURE if exists mnts.pns_web_menu_rebuild;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE mnts.pns_web_menu_rebuild()
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''

BEGIN
	declare start_id int;
	
	CALL setupProcLog();
	
-- Get the starter Node: The Parent ID is set to the ent id
	select ent_id into start_id 
	from	mnts.ent
	where ent.ent_grp_id = 8
	and	ent.parent_id > 0
	and	ent.ent_id = ent.parent_id;
CALL procLog(concat('Start ID = ', ifnull(start_id,'-')));

-- Initialize all the nodes for Web Pages 			
	update mnts.ent
 	 	set lft = 0,
 	 		rgt = 0
 	where ent_grp_id = 8;

	update mnts.ent
		set lft = 1,
			rgt = 2
	where ent_id = start_id;
	
 	CALL mnts.pns_web_menu_recursive(start_id); 		
   call cleanup("end of proc");
END