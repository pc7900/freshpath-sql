DROP PROCEDURE if exists mnts.pns_ent_grp_rebuild;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE mnts.pns_ent_grp_rebuild()
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''

BEGIN
	declare root_id int;
	DECLARE done INT DEFAULT FALSE;

	
	DECLARE cur CURSOR FOR 
		SELECT ent_grp_id 
	 	FROM mnts.ent_grp 
		WHERE parent_id > 0
		and	parent_id = ent_grp_id;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
	
CALL setupProcLog();

-- Initialize all the nodes for Ent Groups			
	update mnts.ent_grp
 	 	set lft = 0, rgt = 0;

	OPEN cur;
	
	read_loop: LOOP
			FETCH cur INTO root_id;
        	IF done THEN
	  			LEAVE read_loop;
			END IF;
CALL procLog(concat('root id = ', ifnull(root_id,'-')));
-- Seed the left and right values of the root id 
			update mnts.ent_grp 
				set lft = 1, rgt = 2
			where ent_grp_id = root_id;	
	
 			CALL mnts.pns_ent_grp_recursive(root_id);
	END LOOP;
    
	CLOSE cur; 		
   call cleanup("end of proc");
END