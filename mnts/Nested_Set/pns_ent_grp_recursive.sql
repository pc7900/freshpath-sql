DROP PROCEDURE if exists mnts.pns_ent_grp_recursive;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE mnts.pns_ent_grp_recursive(IN i_parent_id INT)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''

BEGIN
    DECLARE tmp_id INT;
    DECLARE tmp_parent INT;
    DECLARE parent_lft INT;
    declare parent_rgt int;
    DECLARE new_nright INT;
    DECLARE new_nleft INT;
    declare node_cnt int;
    DECLARE done INT DEFAULT FALSE;
    
    DECLARE cur CURSOR FOR SELECT ent_grp_id, parent_id 
	 								FROM mnts.ent_grp 
									WHERE parent_id = i_parent_id
									and   ent_grp.lft = 0;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    SET max_sp_recursion_depth = 15;
    
 -- Get the Parent left and right   
    select lft, rgt into parent_lft, parent_rgt
    from		ent_grp
    where   ent_grp_id = i_parent_id;
    
    OPEN cur;
    
    read_loop: LOOP
			FETCH cur INTO tmp_id, tmp_parent;
        	IF done THEN
	  			LEAVE read_loop;
			END IF;
			update ent_grp 
				set lft = parent_lft + 1,
				    rgt = parent_lft + 2
			where ent_grp_id = tmp_id;
-- Update the left and rights on all pertinent nodes.			
			UPDATE ent_grp SET rgt = rgt + 2 WHERE ent_grp_id <> tmp_id and rgt > parent_lft;
	  		UPDATE ent_grp SET lft = lft + 2 WHERE ent_grp_id <> tmp_id and lft > parent_lft;


CALL procLog(concat('-- call pns_ent_grp_recursive ',' ent_grp_id: ',tmp_id));
			if tmp_id <> i_parent_id then  -- this will exclude processing the root node endlessly
        		CALL mnts.pns_ent_grp_recursive(tmp_id);
        	end if;
    END LOOP;
    
	CLOSE cur;
    

END