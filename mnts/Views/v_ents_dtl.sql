create or replace view mnts.v_ents_dtl
as
Select	a.srvr_nm
,			d.value		srvr_class
,			a.srvr_ip
,			a.srvr_dns
,			e.value		ent_class
,			c.ent_type
,			b.ent_grp_nm
,			c.ent_nm
,			c.ent_cmn_nm
,			c.ent_desc
,			b.ent_grp_desc
,			a.srvr_desc
,			a.srvr_class_id
,			a.srvr_id
,			b.ent_grp_id
,			c.ent_id
,			c.sprnt_id
from		mnts.srvr a
left join	mnts.mnts_vals d
on			a.srvr_class_id = d.id
join		mnts.ent_grp b
on			a.srvr_id = b.srvr_id
join		mnts.ent	c
on			b.ent_grp_id = c.ent_grp_id
left join		mnts.mnts_vals e
on			c.ent_class_id = e.id