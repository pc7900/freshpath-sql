create or replace view mnts.v_compare_table_counts_sub
as
select 	'Brands' Entity, count(*) BatchMan, 0 whatsfresh
from 		BatchMan.Brand
union all
select 	'Brands', 0 BatchMan, count(*) whatsfresh
from 		whatsfresh.brands
union all
select 	'Ingredients' Entity, count(*) BatchMan, 0 whatsfresh
from 		BatchMan.Ingredient
union all
select 	'Ingredients', 0 BatchMan, count(*) whatsfresh
from 		whatsfresh.ingredients
union all
select 	'Products' Entity, count(*) BatchMan, 0 whatsfresh
from 		BatchMan.Product
union all
select 	'Products', 0 BatchMan, count(*) whatsfresh
from 		whatsfresh.products
union all
select 	'Tasks' Entity, count(*) BatchMan, 0 whatsfresh
from 		BatchMan.Task
union all
select 	'Tasks', 0 BatchMan, count(*) whatsfresh
from 		whatsfresh.tasks
union all
select 	'Product Batch Ingredients' Entity, count(*) BatchMan, 0 whatsfresh
from 		BatchMan.Product_Batch_Ingredient
union all
select 	'Product Batch Ingredients', 0 BatchMan, count(*) whatsfresh
from 		whatsfresh.product_batch_ingredients
union all
select 	'Vendors' Entity, count(*) BatchMan, 0 whatsfresh
from 		BatchMan.Vendor
union all
select 	'Vendors', 0 BatchMan, count(*) whatsfresh
from 		whatsfresh.vendors
union all
select 	'Workers' Entity, count(*) BatchMan, 0 whatsfresh
from 		BatchMan.Worker
union all
select 	'Workers', 0 BatchMan, count(*) whatsfresh
from 		whatsfresh.workers
union all
select 	'Ingredient Batches' Entity, count(*) BatchMan, 0 whatsfresh
from 		BatchMan.Ingredient_Batch
union all
select 	'Ingredient Batches', 0 BatchMan, count(*) whatsfresh
from 		whatsfresh.ingredient_batches
union all
select 	'Product Batches' Entity, count(*) BatchMan, 0 whatsfresh
from 		BatchMan.Product_Batch
union all
select 	'Product Batches', 0 BatchMan, count(*) whatsfresh
from 		whatsfresh.product_batches
union all
select 	'Product Batch Tasks' Entity, count(*) BatchMan, 0 whatsfresh
from 		BatchMan.Product_Batch_Task
union all
select 	'Product Batch Tasks', 0 BatchMan, count(*) whatsfresh
from 		whatsfresh.product_batch_tasks
union all
select 	'Sales' Entity, count(*) BatchMan, 0 whatsfresh
from 		BatchMan.Sales
union all
select 	'Sales', 0 BatchMan, count(*) whatsfresh
from 		whatsfresh.sales

