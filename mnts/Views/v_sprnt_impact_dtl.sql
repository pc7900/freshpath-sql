create or replace view mnts.v_sprnt_impact_dtl
as
Select	a.id
,			concat(lpad(d.sprnt_nbr,2,'0'),' - ',d.sprnt_nm) sprint
,			c.srvr_nm
,			c.srvr_class
,			c.ent_type
,			c.ent_grp_nm
,			c.ent_nm
,			c.ent_cmn_nm
,			lpad(d.sprnt_nbr,2,'0')					sprint_nbr
,			a.impact_desc
,			c.ent_desc
,			c.ent_grp_desc
,			d.sprnt_desc
,			c.ent_class
,			d.trgt_dply_dt
,			c.ent_grp_id
,			d.sprnt_nbr
,			c.ent_id
,			a.sprnt_id
from		mnts.sprnt_impacts 			a
join		mnts.v_ents_dtl				c
on			a.impacted_id = c.ent_id
join		mnts.sprnt						d
on			a.sprnt_id	= d.sprnt_id
			