create or replace view mnts.v_web_menu_sprints
as
Select  v.ent_nm
,	v.ent_cmn_nm
,  v.nav_to
,	s.sprint_nbr
,	v.hier_ordr
, 	v.path
,	v.ent_id
,	s.id			sprnt_impact_id
,	s.sprnt_id
from    mnts.vns_web_menu_hier v
left join mnts.v_sprnt_impact_dtl s
on		v.ent_id = s.ent_id	
order by v.hier_ordr