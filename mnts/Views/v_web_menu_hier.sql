create or replace view mnts.vns_web_menu_hier
as
SELECT 	node.ent_cmn_nm
,			node.ent_desc
,			node.ent_type  nav_to
,		  GROUP_CONCAT(parent.ordr ORDER BY parent.lft ASC SEPARATOR '.') as hier_ordr
,		  GROUP_CONCAT(parent.abbrev_nm ORDER BY parent.lft ASC SEPARATOR ' > ') as path
, 			node.ent_id
FROM ent AS node
     CROSS JOIN ent AS parent 
WHERE 
  node.lft BETWEEN parent.lft AND parent.rgt 
  and node.ent_grp_id = 8 -- Web Menu Items
  and node.parent_id > 0
GROUP by  node.ent_id 
ORDER BY 4;