create or replace view mnts.vns_ent_grp_hier
as
SELECT 	node.ent_grp_nm  foldr
,		  GROUP_CONCAT(parent.ent_grp_nm ORDER BY parent.lft ASC SEPARATOR '/') as path
,			(COUNT(parent.ent_grp_id) - 1) AS depth
,			node.ent_grp_desc	foldr_desc
, 			node.ent_grp_id
,			node.parent_id
,			node.lft
,			node.rgt 
FROM 		ent_grp AS node
CROSS JOIN ent_grp AS parent 
WHERE 
  node.lft BETWEEN parent.lft AND parent.rgt 
  and node.parent_id > 0
GROUP by 	node.ent_grp_id 