CREATE or REPLACE VIEW mnts.v_ents as
select 	b.ent_grp_id
,			cast(rout.ROUTINE_TYPE as char(20))	obj_type
,			rout.ROUTINE_NAME 				obj_nm
,			''										obj_engine
,			null									obj_rows
,			null									obj_incr
,			rout.ROUTINE_COMMENT 			obj_desc
,			rout.CREATED 						created_at
,			rout.LAST_ALTERED				 	updated_at
from 		information_schema.ROUTINES	rout
join     mnts.ent_grp b
on		   rout.ROUTINE_SCHEMA = b.ent_grp_nm
union
select 	c.ent_grp_id
,			'TRIGGER'							obj_type
,			trig.TRIGGER_NAME 				obj_nm
,			''										obj_engine
,			null									obj_rows
,			null									obj_incr
,			concat('Capture ', trig.ACTION_TIMING, ' ', trig.EVENT_MANIPULATION, ' on ', trig.EVENT_OBJECT_TABLE) obj_desc
,			trig.CREATED 						created_at
,			null								 	updated_at
from 		information_schema.TRIGGERS	trig
join		mnts.ent_grp c
on			trig.TRIGGER_SCHEMA = c.ent_grp_nm
union
select 	d.ent_grp_id
,			case when tbl.TABLE_TYPE = 'BASE TABLE' then 'TABLE' else 'VIEW' end AS obj_type
,			tbl.TABLE_NAME AS obj_nm
,			tbl.ENGINE AS obj_engine
,			tbl.TABLE_ROWS AS obj_rows
,			tbl.AUTO_INCREMENT AS obj_incr
,			tbl.TABLE_COMMENT AS obj_desc
,			tbl.CREATE_TIME AS created_at
,			tbl.UPDATE_TIME AS updated_at
from 		information_schema.TABLES tbl 
join		mnts.ent_grp d
on			tbl.TABLE_SCHEMA = d.ent_grp_nm
order by 1,2,3