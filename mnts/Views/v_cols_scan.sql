create or replace view mnts.v_col_scan
as
SELECT
			b.ent_id	
,			cast(colm.ORDINAL_POSITION as dec(5,0)) 	ordr    
,			colm.COLUMN_NAME			col_nm
,			case 	when colm.COLUMN_KEY = 'PRI' then 'PK'
 				when colm.COLUMN_KEY = 'MUL' then 'Ky'
				else ''
			end											col_Key
,			colm.COLUMN_DEFAULT						default_val
,			colm.IS_NULLABLE							nulls
,			colm.DATA_TYPE								col_dtype
,			colm.CHARACTER_MAXIMUM_LENGTH			col_lngth
,			colm.COLUMN_TYPE							col_type
,			colm.COLUMN_COMMENT						col_desc
FROM 		mnts.ent_grp a
join		mnts.ent		b
on			a.ent_grp_id = b.ent_grp_id
join		INFORMATION_SCHEMA.COLUMNS colm
on			colm.TABLE_SCHEMA = a.ent_grp_nm
and  		colm.TABLE_NAME = b.ent_nm


