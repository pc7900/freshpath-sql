create or replace view mnts.v_dependancies
as
SELECT  tab.TABLE_SCHEMA 		ent_grp_nm
,tab.TABLE_NAME 		ent_nm
,case when tab.TABLE_TYPE = 'BASE TABLE' then 'TABLE' else tab.TABLE_TYPE end  ent_type
,views.TABLE_SCHEMA	dep_ent_grp_nm
,'VIEW'					dep_ent_type
,views.TABLE_NAME	 	dep_ent_nm
,ents.ent_id
,d_ents.ent_id			dep_ent_id
FROM information_schema.`TABLES` 		tab
join	mnts.v_ents_dtl						ents
on	tab.TABLE_SCHEMA = ents.ent_grp_nm
and	tab.TABLE_NAME	= ents.ent_nm 
and	ents.srvr_id = 1		-- MySQL - not MS Access
INNER JOIN information_schema.VIEWS 	views 
ON instr(views.VIEW_DEFINITION , concat('`',tab.TABLE_NAME,'`')) > 0
and instr(views.VIEW_DEFINITION, concat('`',tab.TABLE_SCHEMA,'`')) > 0
join	mnts.v_ents_dtl						d_ents
on		views.TABLE_SCHEMA = d_ents.ent_grp_nm
and	views.TABLE_NAME		= d_ents.ent_nm
where		tab.TABLE_SCHEMA in ('whatsfresh')
union all 
SELECT  tab.TABLE_SCHEMA 		ent_grp_nm
,tab.TABLE_NAME 		ent_nm
,case when tab.TABLE_TYPE = 'BASE TABLE' then 'TABLE' else tab.TABLE_TYPE end  ent_type
,rtn.ROUTINE_SCHEMA	dep_ent_grp_nm
,rtn.ROUTINE_TYPE		dep_ent_type
,rtn.ROUTINE_NAME	 	dep_ent_nm
,ents.ent_id
,d_ents.ent_id							dep_ent_id
FROM information_schema.`TABLES` 			AS tab 
join	mnts.v_ents_dtl						ents
on	tab.TABLE_SCHEMA = ents.ent_grp_nm
and	tab.TABLE_NAME	= ents.ent_nm 
and	ents.srvr_id = 1			-- MySQL - not MS Access
INNER JOIN information_schema.ROUTINES 	AS rtn
ON instr(rtn.ROUTINE_DEFINITION, concat(tab.TABLE_SCHEMA,'.',tab.TABLE_NAME)) > 0
join	mnts.v_ents_dtl						d_ents
on		rtn.ROUTINE_SCHEMA = d_ents.ent_grp_nm
and	rtn.ROUTINE_NAME		= d_ents.ent_nm
where	tab.TABLE_SCHEMA in ('whatsfresh')
ORDER BY 1,2,3,4