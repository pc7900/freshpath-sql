create or replace view mnts.v_cols_dtl
as
SELECT 	b.ent_grp_nm
,			c.ent_nm
,			c.ent_type
,			d.attr_ordr
,			d.attr_nm
,			d.attr_cmn_nm
,			d.attr_format
,			d.attr_dflt
,			d.attr_nullable
,			d.attr_key
,			d.attr_desc
,			date_format(d.create_dt,'%m-%e-%Y') create_dt
,			date_format(d.change_dt,'%m-%e-%Y')	change_dt
,			d.detected
,			b.srvr_id
,			b.ent_grp_id
,			c.ent_id
,			d.attr_id
FROM 		mnts.srvr a
join		mnts.ent_grp b
on			a.srvr_id = b.srvr_id
join		mnts.ent c
on			b.ent_grp_id = c.ent_grp_id
left join mnts.attr d
on			c.ent_id = d.ent_id
where    a.srvr_id = 1
order by b.ent_grp_nm
,			c.ent_nm
,			c.ent_type
,			d.attr_ordr
,			d.attr_nm
