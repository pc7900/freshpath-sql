create or replace view mnts.v_compare_table_counts
as
select 	Entity
,			sum(BatchMan) 	BatchMan
,			sum(whatsfresh) whatsfresh
from mnts.v_compare_table_counts_sub
group by 1