create or replace view mnts.v_dependancies_dtl
as
select 	ent.ent_grp_nm
,			ent.ent_nm
,			ent.ent_type
,			ent.ent_class
,			ent_d.ent_type		dep_ent_type
,			ent_d.ent_grp_nm	dep_ent_grp_nm
,			ent_d.ent_nm		dep_ent_nm
,			ent.ent_id
,			ent_d.ent_id		dep_ent_id
from 		mnts.v_dependancies 	dep
join		mnts.v_ents_dtl		ent
on			ent.ent_grp_nm 	= dep.ent_grp_nm
and		ent.ent_nm			= dep.ent_nm
join		mnts.v_ents_dtl		ent_d
on			ent_d.ent_grp_nm 	= dep.dep_ent_grp_nm
and		ent_d.ent_nm		= dep.dep_ent_nm	
order by ent.ent_grp_nm
,			ent.ent_nm
,			ent_d.ent_grp_nm
,			ent_d.ent_nm