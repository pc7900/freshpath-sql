create or replace view mnts.v_app_intrfc_dtl
as
select	b.intrfc_id
,			a.app_nm
,			d.srvr_nm
,			e.value			srvr_class
,			c.ent_grp_nm
,			src_d.srvr_nm		src_srvr_nm		
,			src_e.value			src_srvr_class
,			src_c.ent_grp_nm	src_ent_grp_nm
,			b.intrfc_desc
,			a.app_desc
,			d.srvr_desc
,			c.ent_grp_desc
,			a.app_id
,			c.ent_grp_id
,			b.src_ent_grp_id			
from		mnts.app 			a
join		mnts.app_intrfc 	b
on			a.app_id		= b.app_id
join		mnts.ent_grp		c
on			b.ent_grp_id	= c.ent_grp_id
join		mnts.srvr			d
on			c.srvr_id		= d.srvr_id
join		mnts.mnts_vals		e
on			d.srvr_class_id = e.id
left join		mnts.ent_grp		src_c
on			b.src_ent_grp_id	= src_c.ent_grp_id
left join		mnts.srvr			src_d
on			src_c.srvr_id		= src_d.srvr_id
left join		mnts.mnts_vals		src_e
on			src_d.srvr_class_id = src_e.id
order by a.app_nm
,			e.value
,			c.ent_grp_nm