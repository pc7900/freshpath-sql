DROP FUNCTION IF EXISTS mnts.get_user_id;
DELIMITER $$
CREATE FUNCTION mnts.get_user_id  (i_user_val VARCHAR(20))

	RETURNS INT READS SQL DATA
	COMMENT 'IN: BatchMan User Name   OUT: user_id'
BEGIN
	declare o_user_id INT;
	declare o_user_nm VARCHAR(18);
	
	if isnull(i_user_val) = 1 then
		set o_user_id = 6;
	else
		case 
			when INSTR(i_user_val,':') then
				set o_user_nm = substr(i_user_val, 1, INSTR(i_user_val,':') -1);
			when INSTR(i_user_val,'@') then
				set o_user_nm = substr(i_user_val, 1, INSTR(i_user_val,'@') -1); 
			when INSTR(i_user_val,' ') then
				set o_user_nm = substr(i_user_val, 1, INSTR(i_user_val,' ') -1);
			else
				set o_user_nm = i_user_val;
		end case;
			 
		select user_id into o_user_id
		from BatchMan.BMan_User
		where user_first_nm = o_user_nm;

		if isnull(o_user_nm) = 1 then
			set o_user_id = 6;
		end if;
	end if;

	return o_user_id;
END;
$$