DROP PROCEDURE if exists batch_man.sp_Prd_Btch_Ingr_INSERT;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE batch_man.sp_Prd_Btch_Ingr_INSERT(
	IN i_pbi_id 	INT,
	IN i_ib_id 		INT,
	in i_usr			int,
	IN `o_action` VARCHAR(20)
)

LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'This routine will insert an Ingredient Batch in the case where 2 ingredient batches are used for same Ingredient in a Product Batch.'
BEGIN
  	declare o_pbi_id    int;
  	declare o_pr_id     int;  -- prd_ingr_id (recipe ingredient)
  	declare o_ib_id     int;


   select  a.product_batch_id, a.product_recipe_id
   into    o_pbi_id, o_pr_id
   from    batch_man.product_batch_ingredients a
   where   prd_btch_ingr_id = i_pbi_id;
    
-- Insert the Ingredient Batch to Add	
		insert into batch_man.product_batch_ingredients (product_batch_id, ingredient_batch_id, product_recipe_id, create_by) 
    	values      	(i_pb_id, i_ib_id, o_pr_id, i_usr)
		 on duplicate key
-- On Duplicate key we don't want to do anything.  But we have to do something.
		 	update update_by = i_usr;
   set o_action = 'Insert';
  
END