DROP PROCEDURE if exists batch_man.sp_Prd_Btch_Ingr_UPDATE;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE batch_man.sp_Prd_Btch_Ingr_UPDATE(
	IN i_pbi_id 		int,
   IN i_ib_id      	int,
   IN i_usr        	int,
	OUT o_action 		varchar(20)    
 
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'This routine will Update the selected Product Batch Ingredient with the Ingredient Batch passed in.'
BEGIN
      update batch_man.product_batch_ingredients
      set     ingredient_batch_id = i_ib_id
      ,       change_at = NOW()
      ,       change_by = i_usr
      where   id = i_pbi_id;
      set o_action = 'Update';
    
END
$$