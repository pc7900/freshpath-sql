DROP PROCEDURE if exists batch_man.sp_Prd_Btch_Ingr_CLEAR;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE batch_man.sp_Prd_Btch_Ingr_CLEAR(
	IN i_pbi_id int,
	IN i_usr int,
	OUT o_action varchar(20)    
 
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'This routine will clear the given Ingredient Batch from the Product Batch Ingredient table.'
BEGIN
/* 
  Area:  Product Batch Ingredient
  Routine:  Product Batch Ingredient CLEAR Ingredient Batch
*/
   update batch_man.product_batch_ingredients 
   set     ingredient_batch_id = 0
   ,       change_at = NOW()
   ,       change_by = i_usr
   where   id = i_pbi_id;
   
   set o_action = 'Update';
     
END
$$