DROP PROCEDURE if exists batch_man.sp_Prd_Btch_Ingr_DELETE;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE batch_man.sp_Prd_Btch_Ingr_DELETE(
	IN `i_pbi_id` INT,
	IN `i_usr` VARCHAR(30),
	OUT `o_action` VARCHAR(20)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Deletes a Selected Product Batch Ingredient row'
BEGIN
/*  
Area:  		Product Batch Ingredient
Function:	Given a Prd_Btch_Ingr_ID, this routine will delete the row 
This routine will Delete the selected row from 
*/
	delete 
	from batch_man.product_batch_ingredients
   where   id = i_pbi_id;
   set 	o_action = 'Delete';
END
$$