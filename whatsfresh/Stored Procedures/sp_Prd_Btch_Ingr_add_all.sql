DROP PROCEDURE if exists batch_man.sp_Prd_Btch_Ingr_add_all;
DELIMITER $$

CREATE DEFINER=`BatchMan`@`%` PROCEDURE batch_man.sp_Prd_Btch_Ingr_add_all(
	IN `i_pb_id` INT,
	IN `i_prd_id` INT,
	IN `i_usr_id` int,
	OUT `o_action` INT

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'This routine will insert all the Ingredients in a Product Recipe into the Product Batch Ingredients table for a product batch'
BEGIN
	declare o_pr_id     		int; 
  	declare o_prd_id    		int;
  	declare o_ingr_id   		int;
  	declare rec_cnt				int default 0;
  	declare finished boolean 	default false;
	declare csr cursor for
		Select 		pr.id
		,				pr.product_id
		, 				pr.ingredient_id
		from 	 		batch_man.product_recipes 				pr
		where 		pr.product_id = i_prd_id
		and			pr.active <> 0;           -- Must be active Ingredient
		
	declare continue handler for not found set finished = true;
 
  -- Loop through all the Ingredients in the Curser and add them to the Product Batch Ingredient table  
 	open csr;
	cloop: loop
		fetch csr into o_pr_id, o_prd_id, o_ingr_id;
		if finished then
			close csr;
			set finished = false;
			leave cloop;
		end if;
		

-- Insert the missing Ingredients		
		insert into batch_man.product_batch_ingredients (product_batch_id, ingredient_batch_id, product_recipe_id, create_by) 
    	values      	(i_pb_id, 0, o_pr_id, i_usr)
		 on duplicate key
-- On Duplicate key we don't want to do anything.  But we have to do something.
		 	update rec_cnt = rec_cnt;
		set rec_cnt = rec_cnt + 1;

		
	end loop cloop;
   set o_action = rec_cnt;

END